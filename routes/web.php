<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/get-att-log',function(){
	return \App\Services\AttLog::getAttLog(['192.168.0.18']);
});

Auth::routes();

Route::group(['prefix'=>'unit-kerja'],function(){
	Route::get('/get-combobox-value','UnitKerjaController@comboboxValue');
});

Route::group(['prefix'=>'jabatan'],function(){
	Route::get('/get-combobox-value','JabatanController@comboboxValue');
});

Route::group(['prefix'=>'pegawai'],function(){
	Route::get('/get-combobox-value','PegawaiController@comboboxValue');
	Route::get('/list-data','PegawaiController@listData');
});

Route::group(['middleware' => ['auth']], function() 
{
	Route::get('/','HomeController@index');
	
	Route::get('/pengajuan-absen-manual/update-status','PengajuanAbsenManualController@updateStatus');
	Route::resource('/pengajuan-absen-manual','PengajuanAbsenManualController');
	Route::post('/pengajuan-absen-manual/datatable','PengajuanAbsenManualController@datatable');
	
}); 

Route::group(['middleware' => ['auth','role:admin']], function() 
{
	Route::resource('/satuan-kerja','SatuanKerjaController');
	Route::post('/satuan-kerja/datatable','SatuanKerjaController@datatable');
	
	Route::resource('/unit-kerja','UnitKerjaController');
	Route::post('/unit-kerja/datatable','UnitKerjaController@datatable');
	
	Route::resource('/pegawai','PegawaiController');
	Route::post('/pegawai/datatable','PegawaiController@datatable');
	
	Route::resource('/jabatan','JabatanController');
	Route::post('/jabatan/datatable','JabatanController@datatable');
	
	Route::resource('/mesin-absen','MesinAbsenController');
	Route::post('/mesin-absen/datatable','MesinAbsenController@datatable');
	
	Route::resource('/hari-libur','HariLiburController');
	Route::post('/hari-libur/datatable','HariLiburController@datatable');
	
	Route::resource('/jam-kerja','JamKerjaController');
	Route::post('/jam-kerja/datatable','JamKerjaController@datatable');
	
	Route::resource('/absen-harian','AbsenHarianController');
	Route::post('/absen-harian/datatable','AbsenHarianController@datatable');
	
	Route::resource('/jam-kerja-pegawai','JamKerjaPegawaiController');
	Route::post('/jam-kerja-pegawai/datatable','JamKerjaPegawaiController@datatable');
	
	Route::resource('/pin-absen','PinAbsenController');
	Route::post('/pin-absen/datatable','PinAbsenController@datatable');
	
});

Route::group(['prefix' => 'iclock', 'middleware' => ['validatesn']], function() {
	Route::get('cdata', 'IClockController@cdataget');
	Route::post('cdata', 'IClockController@cdatapost');
	Route::get('devicecmd', 'IClockController@devicecmd');
	Route::get('getrequest', 'IClockController@getrequest');
});


