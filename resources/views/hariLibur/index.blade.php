@extends('layout.layout')

@section('content')

<div class="page-title">
    <div class="title_left">
        <h3>Master Hari Libur</h3>
    </div>
	
</div>
<div class="clearfix"></div>

<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>Data Hari Libur</h2>
                    <ul class="nav navbar-right panel_toolbox">
						<li><a href="{{ Url('hari-libur/create') }}" class="btn btn-info"><i class="fa fa-plus"></i> Tambah Data</a>&nbsp;&nbsp;</li>
					</ul>
					<div class="clearfix"></div>
            </div>
            <div class="x_content">
                <table id="myTable" class="table table-striped table-bordered dt-responsive" width="100%">
                    <thead>
						<tr>
                          <th>No</th>
						  <th>Tanggal</th>
						  <th>Keterangan</th>
						  <th>Oleh</th>
						  <th></th>
                         
                        </tr>
                    </thead>
					  
				</table>
			</div>
		</div>
	</div>
</div>
@include('scripts.datatable')
<script>
	
	$(function(){
		table = $('#myTable').DataTable({
			"processing": true,
			"serverSide": true,
			"ajax": {
				"url": "{{ Url('hari-libur/datatable') }}",
				"type":"POST",
				"data": function ( d ) {
					
				}
			},
			"columns": [
				{ "data": 'DT_RowIndex',"searchable": false},
				{ "data": "tanggal" },
				{ "data": "keterangan" },
				{ "data": "oleh" },
				{ "data": "action"}
			],
			"initComplete": function(settings, json) {
				$('[data-toggle="tooltip"]').tooltip();
			},
			"order": [[1, 'desc']],
		});
		
		$('#myTable').on('click', '.btn-delete', function (e) { 
			e.preventDefault();
			var id = $(this).data('id');
			$_confirm(function(){
				$.ajax({
					url: "{{ Url('hari-libur') }}/"+id,
					type: 'DELETE',
					dataType: 'json',
					data: {method: '_DELETE', submit: true}
				}).always(function (data) {
					if (data.responseText == 'success'){
						table.draw(false);
					} else {
						swal(data.responseText);
					}
				});
			});
		});
	});
</script>

@endsection