@extends('layout.layout')
@section('content')
<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>Form Hari libur</h2>
                    <a href="{{ Url('hari-libur') }}" class="btn btn-info pull-right"><i class="fa fa-arrow-left"></i> Kembali</a>
					<div class="clearfix"></div>
            </div>
            <div class="x_content">
				@include('scripts.alert')
				{!! BootForm::horizontal(['model' => $model, 'store' => 'HariLiburController@store', 'update' => 'HariLiburController@update']) !!}
				{!! BootForm::text('tanggal',null,null,['class'=>'datepicker']) !!}
				{!! BootForm::text('keterangan') !!}
				{!! BootForm::submit('Simpan') !!}
				{!! BootForm::close() !!}
			</div>
		</div>
	</div>
</div>
<script>
   var CURRENT_URL = "{{ Url('hari-libur') }}";
</script>
@endsection