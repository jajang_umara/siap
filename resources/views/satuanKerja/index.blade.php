@extends('layout.layout')

@section('content')

<div class="page-title">
    <div class="title_left">
        <h3>Master Satuan Kerja</h3>
    </div>
	
</div>
<div class="clearfix"></div>

<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>Data Satuan Kerja</h2>
                    <ul class="nav navbar-right panel_toolbox">
						<li><a href="{{ Url('satuan-kerja/create') }}" class="btn btn-info"><i class="fa fa-plus"></i> Tambah Data</a>&nbsp;&nbsp;</li>
					</ul>
					<div class="clearfix"></div>
            </div>
            <div class="x_content">
                <table id="myTable" class="table table-striped table-bordered dt-responsive" width="100%">
                    <thead>
						<tr>
                          <th>No</th>
						  <th>Nama Satuan Kerja</th>
						  <th>Alamat</th>
						  <th></th>
                         
                        </tr>
                    </thead>
					  
				</table>
			</div>
		</div>
	</div>
</div>
@include('scripts.datatable')
<script>
	
	$(function(){
		table = $('#myTable').DataTable({
			"processing": true,
			"serverSide": true,
			"ajax": {
				"url": "{{ Url('satuan-kerja/datatable') }}",
				"type":"POST",
				"data": function ( d ) {
					
				}
			},
			"columns": [
				{ "data": 'DT_RowIndex',"searchable": false},
				{ "data": "nama_satuan_kerja" },
				{ "data": "alamat" },
				{ "data": "action"}
			],
			"initComplete": function(settings, json) {
				$('[data-toggle="tooltip"]').tooltip();
			},
			"order": [[1, 'asc']],
		});
		
		$('#myTable').on('click', '.btn-delete', function (e) { 
			e.preventDefault();
			var id = $(this).data('id');
			$_confirm(function(){
				$.ajax({
					url: "{{ Url('satuan-kerja') }}/"+id,
					type: 'DELETE',
					dataType: 'json',
					data: {method: '_DELETE', submit: true}
				}).always(function (data) {
					if (data.responseText == 'success'){
						table.draw(false);
					} else {
						swal(data.responseText);
					}
				});
			});
		});
	});
</script>

@endsection