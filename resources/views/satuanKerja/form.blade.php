@extends('layout.layout')
@section('content')
<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>Form Satuan Kerja</h2>
                    <a href="{{ Url('satuan-kerja') }}" class="btn btn-info pull-right"><i class="fa fa-arrow-left"></i> Kembali</a>
					<div class="clearfix"></div>
            </div>
            <div class="x_content">
				@if (session('status'))
					<div class="alert alert-success">
						{{ session('status') }}
					</div>
				@endif
				@if (session('error'))
					<div class="alert alert-danger">
						{{ session('error') }}
					</div>
				@endif
				{!! BootForm::horizontal(['model' => $satuanKerja, 'store' => 'SatuanKerjaController@store', 'update' => 'SatuanKerjaController@update']) !!}
				{!! BootForm::text('nama_satuan_kerja') !!}
				{!! BootForm::text('alamat') !!}
				{!! BootForm::submit('Simpan') !!}
				{!! BootForm::close() !!}
			</div>
		</div>
	</div>
</div>
<script>
   var CURRENT_URL = "{{ Url('satuan-kerja') }}";
</script>
@endsection