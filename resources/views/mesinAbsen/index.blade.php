@extends('layout.layout')

@section('content')

<div class="page-title">
    <div class="title_left">
        <h3>Master mesin Absen</h3>
    </div>
	
</div>
<div class="clearfix"></div>

<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>Data Mesin Absen</h2>
                    <ul class="nav navbar-right panel_toolbox">
						<li><a href="{{ Url('mesin-absen/create') }}" class="btn btn-info"><i class="fa fa-plus"></i> Tambah Data</a>&nbsp;&nbsp;</li>
					</ul>
					<div class="clearfix"></div>
            </div>
            <div class="x_content">
                <table id="myTable" class="table table-striped table-bordered dt-responsive" width="100%">
                    <thead>
						<tr>
                          <th>No</th>
						  <th>Nama mesin</th>
						  <th>Merk</th>
						  <th>Type</th>
						  <th>Serial Number</th>
						  <th>Ip Address</th>
						  <th></th>
                         
                        </tr>
                    </thead>
					  
				</table>
			</div>
		</div>
	</div>
</div>
@include('scripts.datatable')
<script>
	
	$(function(){
		table = $('#myTable').DataTable({
			"processing": true,
			"serverSide": true,
			"ajax": {
				"url": "{{ Url('mesin-absen/datatable') }}",
				"type":"POST",
				"data": function ( d ) {
					
				}
			},
			"columns": [
				{ "data": 'DT_RowIndex',"searchable": false},
				{ "data": "nama_mesin" },
				{ "data": "merk" },
				{ "data": "type" },
				{ "data": "serial_number" },
				{ "data": "ip_address" },
				{ "data": "action"}
			],
			"initComplete": function(settings, json) {
				$('[data-toggle="tooltip"]').tooltip();
			},
			"order": [[1, 'asc']],
		});
		
		$('#myTable').on('click', '.btn-delete', function (e) { 
			e.preventDefault();
			var id = $(this).data('id');
			$_confirm(function(){
				$.ajax({
					url: "{{ Url('satuan-kerja') }}/"+id,
					type: 'DELETE',
					dataType: 'json',
					data: {method: '_DELETE', submit: true}
				}).always(function (data) {
					if (data.responseText == 'success'){
						table.draw(false);
					} else {
						swal(data.responseText);
					}
				});
			});
		});
	});
</script>

@endsection