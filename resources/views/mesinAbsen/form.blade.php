@extends('layout.layout')
@section('content')
<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>Form Mesin Absen</h2>
                    <a href="{{ Url('mesin-absen') }}" class="btn btn-info pull-right"><i class="fa fa-arrow-left"></i> Kembali</a>
					<div class="clearfix"></div>
            </div>
            <div class="x_content">
				@if (session('status'))
					<div class="alert alert-success">
						{{ session('status') }}
					</div>
				@endif
				@if (session('error'))
					<div class="alert alert-danger">
						{{ session('error') }}
					</div>
				@endif
				{!! BootForm::horizontal(['model' => $model, 'store' => 'MesinAbsenController@store', 'update' => 'MesinAbsenController@update']) !!}
				{!! BootForm::text('nama_mesin') !!}
				{!! BootForm::text('merk') !!}
				{!! BootForm::text('type') !!}
				{!! BootForm::text('serial_number') !!}
				{!! BootForm::text('ip_address') !!}
				{!! BootForm::submit('Simpan') !!}
				{!! BootForm::close() !!}
			</div>
		</div>
	</div>
</div>
<script>
   var CURRENT_URL = "{{ Url('mesin-absen') }}";
</script>
@endsection