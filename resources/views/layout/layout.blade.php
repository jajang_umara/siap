<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="csrf-token" content="{{ csrf_token() }}" />
	<link rel="icon" href="images/favicon.ico" type="image/ico" />

    <title>SIAP DASHBOARD</title>

    <!-- Bootstrap -->
    <link href="{{ asset('public/assets/vendors/bootstrap/dist/css/bootstrap.min.css') }}" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="{{ asset('public/assets/vendors/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet">
	<link href="{{ asset('public/assets/vendors/nprogress/nprogress.css') }}" rel="stylesheet">
    <!-- iCheck -->
    <link href="{{ asset('public/assets/vendors/iCheck/skins/flat/green.css') }}" rel="stylesheet">
	<!-- Switchery -->
    <link href="{{ asset('public/assets/vendors/switchery/dist/switchery.min.css') }}" rel="stylesheet">
	<link href="{{ asset('public/assets/vendors/pnotify/dist/pnotify.css') }}" rel="stylesheet">
    <link href="{{ asset('public/assets/vendors/pnotify/dist/pnotify.buttons.css') }}" rel="stylesheet">
    <link href="{{ asset('public/assets/vendors/pnotify/dist/pnotify.nonblock.css') }}" rel="stylesheet">
	<link href="{{ asset('public/assets/vendors/jquery-confirm/jquery-confirm.min.css') }}" rel="stylesheet">
	<!-- Custom Theme Style -->
    <link href="{{ asset('public/assets/build/css/custom.min.css') }}" rel="stylesheet">
	<link href="{{ asset('public/assets/vendors/bootstrap-daterangepicker/daterangepicker.css') }}" rel="stylesheet">
    <!-- bootstrap-datetimepicker -->
    <link href="{{ asset('public/assets/vendors/bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.css') }}" rel="stylesheet">
	 <link href="{{ asset('public/assets/vendors/select2/dist/css/select2.min.css') }}" rel="stylesheet">
	 <link href="{{ asset('public/assets/vendors/waitMe/waitMe.min.css') }}" rel="stylesheet">
	 <link href="{{ asset('public/assets/vendors/bootstrap-select/dist/css/bootstrap-select.min.css') }}" rel="stylesheet">
	 <script src="{{ asset('public/assets/vendors/jquery/dist/jquery.min.js') }}"></script>
	<style>
		
		.error {
			color: #a94442;
		}
	</style>
	<script>
		var table;
		$.ajaxSetup({
			headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			}
		});
	</script>
  </head>
 
  <body class="nav-md">
	<div class="container body">
		<div class="main_container">
			@include('layout.header')
			<!-- page content -->
        <div class="right_col" role="main">
			@yield('content')
		</div>
	</div>
	</div>
	
	 <!-- jQuery -->
   
    <!-- Bootstrap -->
    <script src="{{ asset('public/assets/vendors/bootstrap/dist/js/bootstrap.min.js') }}"></script>
	<script src="{{ asset('public/assets/vendors/fastclick/lib/fastclick.js') }}"></script>
    <!-- NProgress -->
    <script src="{{ asset('public/assets/vendors/nprogress/nprogress.js') }}"></script>
    <!-- iCheck -->
    <script src="{{ asset('public/assets/vendors/iCheck/icheck.min.js') }}"></script>
	<!-- Switchery -->
    <script src="{{ asset('public/assets/vendors/switchery/dist/switchery.min.js') }}"></script>
	 <!-- PNotify -->
    <script src="{{ asset('public/assets/vendors/pnotify/dist/pnotify.js') }}"></script>
    <script src="{{ asset('public/assets/vendors/pnotify/dist/pnotify.buttons.js') }}"></script>
    <script src="{{ asset('public/assets/vendors/pnotify/dist/pnotify.nonblock.js') }}"></script>
	<!-- Parsley -->
	<script src="{{ asset('public/assets/vendors/jqueryvalidate/additional-methods.min.js') }}"></script>
    <script src="{{ asset('public/assets/vendors/jqueryvalidate/jquery.validate.min.js') }}"></script>
	<script src="{{ asset('public/assets/vendors/jquery-confirm/jquery-confirm.min.js') }}"></script>
	 
	<script src="{{ asset('public/assets/vendors/select2/dist/js/select2.full.min.js') }}"></script>
	<script src="{{ asset('public/assets/vendors/moment/min/moment.min.js') }}"></script>
	 <script src="{{ asset('public/assets/vendors/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
    <!-- bootstrap-datetimepicker -->    
    <script src="{{ asset('public/assets/vendors/bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js') }}"></script>
	<script src="{{ asset('public/assets/vendors/waitMe/waitMe.min.js') }}"></script>
	<script src="{{ asset('public/assets/vendors/sweetalert/sweetalert2.all.min.js') }}"></script>
	<script src="{{ asset('public/assets/vendors/bootstrap-select/dist/js/bootstrap-select.min.js') }}"></script>
	<!-- Custom Theme Scripts -->
	<script src="{{ asset('public/assets/build/js/custom.min.js') }}"></script>
	<script>
		
		function doLogout()
		{
			var form = $('<form/>').attr('method','post').attr('action',"{{ route('logout') }}");
			form.append('{{ csrf_field() }}');
			form.appendTo('body');
			form.submit();
		}
		function $_confirm(callback)
		{
			$.confirm({
				title: 'Konfirmasi!',
				content: 'Apakah anda yakin akan menghapus data ini?',
				theme: 'supervan',
				buttons: {
					confirm: callback,
					cancel: function () {
					
					},
			
				}
			});
		}
		
		jQuery.validator.setDefaults({
			highlight: function(element) {
			$(element).closest('.form-group').removeClass('has-success').addClass('has-error');
			},
			unhighlight: function(element) {
				$(element).closest('.form-group').removeClass('has-error').addClass('has-success');
			},
			errorPlacement: function ( error, element ) {

				if(element.parent().hasClass('input-group')){
					error.insertAfter( element.parent() );
				}else{
					error.insertAfter( element );
				}

			}
		});
		
		$(function(){
			$('.datepicker').datetimepicker({
				format: 'YYYY-MM-DD',
				ignoreReadonly: true,
			});
			 $('.timepicker').datetimepicker({
				format: 'HH:mm',
				ignoreReadonly: true,
			});
		});
		
		function run_waitMe()
		{
			$('body').waitMe({
				effect : 'win8',
				text : 'Silakan Tunggu...',
				bg : 'rgba(0, 34, 51, 0.7)',
				color : '#000',
				maxSize : '',
				waitTime : -1,
				textPos : 'vertical',
				fontSize : '24px',
				source : '',
			});
				
		}
		
		function stop_waitMe()
		{
			$('body').waitMe("hide");
		}
		
		function optionTree(result,r,p)
		{
			var str = "*";
			if (result.length > 0){
			   r = typeof(r) == 'undefined' ? 0 : r;
			   p = typeof(p) == 'undefined' ? 0 : p;
			   $.each(result,function(k,v){
				   var selected = "";
				   if (parentId == v.unit_kerja_id){
					   selected = "selected";
				   }
				   var dash = (v.parent == 0) ? '' : str.repeat(r)+' ';
				   //var dash = 
				   containerSelect.append("<option value='"+v.unit_kerja_id+"' "+selected+">"+dash+v.nama_unit_kerja+"</option>");
				   if (v.hasOwnProperty('_children')){
					   optionTree(v._children,r+1,v.parent);
				   }
			   });
			}
		}
	</script>
  </body>
</html>
