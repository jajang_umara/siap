 
<div class="col-md-3 left_col">
   <div class="left_col scroll-view">
        <div class="navbar nav_title" style="border: 0;">
              <a href="{{ Url('/') }}" class="site_title"><i class="fa fa-dashboard"></i> <span>SIAP ADMIN</span></a>
            </div>

            <div class="clearfix"></div>

            <!-- menu profile quick info -->
            <div class="profile clearfix">
              <div class="profile_pic">
                <img src="{{ asset('public/assets/images/mau.png') }}" alt="..." class="img-circle profile_img">
              </div>
              <div class="profile_info">
                <span>SELAMAT DATANG, {{ getAuth('username') }}</span>
                <h2></h2>
              </div>
            </div>
            <!-- /menu profile quick info -->

            <br />

            <!-- sidebar menu -->
            <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
              <div class="menu_section">
                <h3>General</h3>
                <ul class="nav side-menu">
                  <li><a href="{{ Url('/') }}"><i class="fa fa-home"></i> Beranda</a></li>
				  <li><a><i class="fa fa-hdd-o"></i> Data Master<span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="{{ Url('satuan-kerja') }}">Master Satuan Kerja</a></li>
                      <li><a href="{{ Url('unit-kerja') }}">Master Unit Kerja</a></li>
                      <li><a href="{{ Url('pegawai') }}">Master Pegawai</a></li>
                      <li><a href="{{ Url('jabatan') }}">Master Jabatan</a></li>
					  <li><a href="{{ Url('mesin-absen') }}">Master Mesin Absen</a></li>
                    </ul>
                  </li>
				  <li><a><i class="fa fa-hdd-o"></i> Menu Utama<span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="form.html">Rekap Kehadiran Pegawai</a></li>
                      <li><a href="form_advanced.html">Detail Kehadiran Pegawai</a></li>
                      <li><a href="form_validation.html">Rekap Prosentase</a></li>
                      <li><a href="form_wizards.html">Daftar Kehadiran Pegawai</a></li>
                      <li><a href="form_upload.html">Rekap Prosentase Harian</a></li>
                      <li><a href="form_buttons.html">Jumlah Kehadiran Harian</a></li>
                    </ul>
                  </li>
				  <li><a><i class="fa fa-users"></i> Manajemen User<span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="form.html">Manajemen Ijin Kerja</a></li>
                      <li><a>Manajemen Ketidakhadiran</a>
						<ul class="nav child_menu">
							<li><a href="form_validation.html">Set Cuti</a></li>
							<li><a href="form_wizards.html">Rekap Cuti</a></li>
							<li><a href="form_upload.html">Detail Cuti</a></li>
					    </ul>
					   </li>
                      <li><a href="{{ Url('hari-libur') }}">Manajemen Hari Libur</a></li>
                      
                      <li><a href="{{ Url('pin-absen') }}">Manajemen Pin Absen</a></li>
                      <li><a href="{{ Url('jam-kerja') }}">Manajemen Jam Kerja</a></li>
                      <li><a href="{{ Url('jam-kerja-pegawai') }}">Pengaturan Jam Kerja Pegawai</a></li>
					  
                      <li><a href="form_upload.html">Pengaturan Jadwal Puasa</a></li>
					  <li><a href="{{ Url('absen-harian') }}">Manajemen Checkinout</a></li>
                      <li><a href="{{ Url('users') }}">Manajemen User</a></li>
                    </ul>
                  </li>
				  <li><a><i class="fa fa-edit"></i> Absensi Manual<span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="{{ Url('pengajuan-absen-manual') }}">Pengajuan Absensi Manual</a></li>
                      <li><a href="form_advanced.html">Entri Absen</a></li>
					</ul>
				  </li>
				   <li><a href="{{ Url('/') }}"><i class="fa fa-cog"></i> Edit Akun</a></li>
				   <li><a href="#" onclick="doLogout()"><i class="fa fa-power-off"></i> Logout</a></li>
                </ul>
              </div>
              

            </div>
            <!-- /sidebar menu -->

            <!-- /menu footer buttons -->
            <div class="sidebar-footer hidden-small">
              <a data-toggle="tooltip" data-placement="top" title="Settings">
                <span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
              </a>
              <a data-toggle="tooltip" data-placement="top" title="FullScreen">
                <span class="glyphicon glyphicon-fullscreen" aria-hidden="true"></span>
              </a>
              <a data-toggle="tooltip" data-placement="top" title="Lock">
                <span class="glyphicon glyphicon-eye-close" aria-hidden="true"></span>
              </a>
              <a data-toggle="tooltip" data-placement="top" title="Logout" href="login.html">
                <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
              </a>
            </div>
            <!-- /menu footer buttons -->
          </div>
        </div>
		
		<!-- top navigation -->
        <div class="top_nav">
          <div class="nav_menu">
            <nav>
              <div class="nav toggle">
                <a id="menu_toggle"><i class="fa fa-bars"></i></a>
              </div>

              <ul class="nav navbar-nav navbar-right">
			  </ul>
            </nav>
          </div>
        </div>