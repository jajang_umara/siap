@extends('layout.layout')
@section('content')
<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>Form Pegawai</h2>
                   <a href="{{ Url('pegawai') }}" class="btn btn-info pull-right"><i class="fa fa-arrow-left"></i> Kembali</a>
					<div class="clearfix"></div>
            </div>
            <div class="x_content">
				@if (session('status'))
					<div class="alert alert-success">
						{{ session('status') }}
					</div>
				@endif
				@if (session('error'))
					<div class="alert alert-danger">
						{{ session('error') }}
					</div>
				@endif
				{!! BootForm::horizontal(['model' => $model, 'store' => 'PegawaiController@store', 'update' => 'PegawaiController@update']) !!}
				{!! BootForm::text('nip_pegawai') !!}
				{!! BootForm::text('nama_pegawai') !!}
				{!! BootForm::email('email_pegawai') !!}
				{!! BootForm::select('satuan_kerja_id','Satuan Kerja',$satuanKerja,null,['onchange'=>'satuanKerjaSelect(this.value)']) !!}
				{!! BootForm::select('unit_kerja_id','Unit Kerja',[],null,['onchange'=>'unitKerjaSelect(this.value)']) !!}
				{!! BootForm::select('jabatan_id','Jabatan') !!}
				{!! BootForm::submit('Simpan') !!}
				{!! BootForm::close() !!}
			</div>
		</div>
	</div>
</div>
<script>
   var CURRENT_URL = "{{ Url('pegawai') }}";
   var parentId = "{{ $model->unit_kerja_id }}";
   var containerSelect =  $('select[name="unit_kerja_id"]');
   
   $(function(){
	  satuanKerjaSelect($('select[name="satuan_kerja_id"]').val()); 
	  
   });
   
   function satuanKerjaSelect(value)
   {
	   $.get("{{ Url('unit-kerja/get-combobox-value') }}",{satuan_kerja_id:value}).done(function(result){
		   var result = JSON.parse(result);
		   containerSelect.empty();
		   containerSelect.append("<option value='0'>Pilih Unit Kerja</option>");
		   optionTree(result);
		   unitKerjaSelect(containerSelect.val());
		   
	   }).fail(function(xhr){
		  swal(xhr.responseText) 
	   });
   }
   
   function unitKerjaSelect(value)
   {
	    $.get("{{ Url('jabatan/get-combobox-value') }}",{unit_kerja_id:value}).done(function(result){
		   var result = JSON.parse(result);
		   $('select[name="jabatan_id"]').empty();
		   $('select[name="jabatan_id"]').append("<option value='0'>Pilih Jabatan</option>");
		   if (result.length > 0){
			   $.each(result,function(k,v){
				   var selected = "";
				   if ("{{ $model->jabatan_id}}" == v.jabatan_id){
					   selected = 'selected';
				   }
				   $('select[name="jabatan_id"]').append("<option value='"+v.jabatan_id+"' "+selected+">"+v.nama_jabatan+"</option>");
			   });
			  
		   }
	   }).fail(function(xhr){
		  swal(xhr.responseText) 
	   });
   }
</script>
@endsection