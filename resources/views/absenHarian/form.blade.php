@extends('layout.layout')
@section('content')
<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>Tarik Absen Harian</h2>
                    <a href="{{ Url('hari-libur') }}" class="btn btn-info pull-right"><i class="fa fa-arrow-left"></i> Kembali</a>
					<div class="clearfix"></div>
            </div>
            <div class="x_content">
				@include('scripts.alert')
				{!! BootForm::horizontal() !!}
				{!! BootForm::text('tanggal_dari',null,null,['class'=>'datepicker']) !!}
				{!! BootForm::text('tanggal_ke',null,null,['class'=>'datepicker']) !!}
				{!! BootForm::select('lokasi_absen',null,$mesin_absen) !!}
				{!! BootForm::button('Proses',['onclick'=>'tarikAbsen()']) !!}
				{!! BootForm::close() !!}
			</div>
		</div>
	</div>
</div>
<script>
   var CURRENT_URL = "{{ Url('absen-harian') }}";
   
   function tarikAbsen()
   {
	   
   }
</script>
@endsection