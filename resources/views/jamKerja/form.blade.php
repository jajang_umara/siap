@extends('layout.layout')
@section('content')
<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>Form Jam Kerja</h2>
                    <a href="{{ Url('jam-kerja') }}" class="btn btn-info pull-right"><i class="fa fa-arrow-left"></i> Kembali</a>
					<div class="clearfix"></div>
            </div>
            <div class="x_content">
				@include('scripts.alert')
				{!! BootForm::vertical(['model' => $model, 'store' => 'JamKerjaController@store', 'update' => 'JamKerjaController@update']) !!}
				<div class="col-md-12">
				{!! BootForm::text('nama_jam_kerja') !!}
				</div>
				<div class="col-md-3">
				{!! BootForm::text('checkin_start',null,null,['class'=>'timepicker']) !!}
				</div>
				<div class="col-md-3">
				{!! BootForm::text('checkin_end',null,null,['class'=>'timepicker']) !!}
				</div>
				<div class="col-md-3">
				{!! BootForm::text('checkout_start',null,null,['class'=>'timepicker']) !!}
				</div>
				<div class="col-md-3">
				{!! BootForm::text('checkout_end',null,null,['class'=>'timepicker']) !!}
				</div>
				<div class="col-md-3">
				{!! BootForm::text('istirahat_start',null,null,['class'=>'timepicker']) !!}
				</div>
				<div class="col-md-3">
				{!! BootForm::text('istirahat_end',null,null,['class'=>'timepicker']) !!}
				</div>
				<div class="col-md-3">
				{!! BootForm::checkbox('is_default') !!}
				</div>
				<div class="col-md-3">
				{!! BootForm::checkbox('is_crossday') !!}
				</div>
				<div class="col-md-12">
				{!! BootForm::submit('Simpan') !!}
				</div>
				{!! BootForm::close() !!}
			</div>
		</div>
	</div>
</div>
<script>
   var CURRENT_URL = "{{ Url('jam-kerja') }}";
</script>
@endsection