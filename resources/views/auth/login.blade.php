<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="icon" href="images/favicon.ico" type="image/ico" />
	<link href="{{ asset('public/assets/vendors/bootstrap/dist/css/bootstrap.min.css') }}" rel="stylesheet">
	<link href="{{ asset('public/css/login.css') }}" rel="stylesheet">
	<script src="{{ asset('public/assets/vendors/jquery/dist/jquery.min.js') }}"></script>
    <title>SIAP DASHBOARD</title>
  </head>
  <body>
  <nav class="navbar navbar-default">
  <div class="container-fluid">
    <center>
      <h3 style="padding:0 0 10px 0">Sistem Informasi Administrasi Presensi (SIAP)</h3>
    </center>
   
  </div>
</nav>
<div class="container">
	<div class="card card-container">
		   <img id="profile-img" class="profile-img-card" src="//ssl.gstatic.com/accounts/ui/avatar_2x.png" />
            <p id="profile-name" class="profile-name-card"></p>
            <form class="form-signin" method="post" action="{{ route('login') }}">
				{{ csrf_field() }}
                
                <input type="text" id="inputEmail" class="form-control" placeholder="Email address" required autofocus name="username">
                <input type="password" id="inputPassword" class="form-control" placeholder="Password" required name="password">
               
                <button class="btn btn-lg btn-primary btn-block btn-signin" type="submit">Sign in</button>
            </form><!-- /form -->
			<small class="text-danger">
            	@if($errors->any())
					<h4>{{$errors->first()}}</h4>
				@endif
			</small>
        </div><!-- /card-container -->
    </div><!-- /container -->


	<footer class="page-footer font-small blue">

  <!-- Copyright -->
  <div class="footer-copyright text-center py-3">© 2019 Copyright:
    <a href="#"> Waroengweb.co.id</a>
  </div>
  <!-- Copyright -->

</footer>
<script>

</script>
</body>
</html>