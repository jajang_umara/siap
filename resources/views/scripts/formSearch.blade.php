{!! BootForm::horizontal() !!}
{!! BootForm::select('satuan_kerja',null,$satuanKerja,null,['onchange'=>'satuanKerjaSelect(this.value)']) !!}
{!! BootForm::select('unit_kerja',null,[],null,['onchange'=>'unitKerjaSelect(this.value)']) !!}
@isset ($pegawai)
{!! BootForm::select('pegawai_select') !!}
@endisset
{!! BootForm::button('<i class="fa fa-search"></i> Cari',['class'=>'do-search btn btn-info']) !!}
{!! BootForm::close()  !!}

<script>
var containerSelect =  $('select[name="unit_kerja"]');
var parentId = "";
$(function(){
	  satuanKerjaSelect($('select[name="satuan_kerja"]').val()); 
	 
   });
function satuanKerjaSelect(value)
   {
	   $.get("{{ Url('unit-kerja/get-combobox-value') }}",{satuan_kerja_id:value}).done(function(result){
		   var result = JSON.parse(result);
		   containerSelect.empty();
		   containerSelect.append("<option value=''>Pilih Unit Kerja</option>");
		   optionTree(result);
	   }).fail(function(xhr){
		  swal(xhr.responseText) 
	   });
   }
   
function unitKerjaSelect(value)
   {
	   $.get("{{ Url('pegawai/get-combobox-value') }}",{unit_kerja_id:value}).done(function(result){
		   result = JSON.parse(result);
		   $('#pegawai_select').empty();
		   $('#pegawai_select').append("<option>Pilih Pegawai</option>");
		   if (Object.keys(result).length > 0){
			  
			    $.each(result,function(k,v){
					 $('#pegawai_select').append("<option value='"+k+"'>"+v+"</option>");
				});
		   }
		  
	   }).fail(function(xhr){
		  swal(xhr.responseText) 
	   });
   }
</script>