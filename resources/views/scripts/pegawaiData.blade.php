<script>
function pegawaiData()
{
	loadTable();
	$('#myModal').modal('show');
}

function loadTable()
{
	$.get("{{ Url('pegawai/list-data') }}",{satuan_kerja:$('#satuan_kerja').val(),'unit_kerja':$('#unit_kerja').val()}).done(function(result){
		$('#tablePegawai tbody').empty();
		result = JSON.parse(result);
		if (result.length > 0){
			$.each(result,function(k,v){
				$('#tablePegawai tbody').append('<tr id="'+v.pegawai_id+'"><td>'+v.nip_pegawai+'</td><td>'+v.nama_pegawai+'</td><td>'+v.satuan_kerja.nama_satuan_kerja+ '</td>'+
												'<td><button class="btn btn-primary btn-xs" onclick="pilihPegawai(this)">Pilih</button></td></tr>');
			});
			
		}
	});
}

function pilihPegawai(that)
{
	pegInput.val($(that).closest('tr').find('td:eq(1)').text());
	$('input[name="pegawai_id"]').val($(that).closest('tr').attr('id'));
	$('#myModal').modal('hide');
}
</script>

<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">DATA PEGAWAI</h4>
      </div>
      <div class="modal-body">
		@include('scripts.formSearch')
        <table class="table table-bordered" id="tablePegawai">
			<thead>
				<tr>
					<th>NIP</th>
					<th>NAMA</th>
					<th>SATUAN KERJA</th>
					<th></th>
				</tr>
			</thead>
			<tbody>
			</tbody>
		</table>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>