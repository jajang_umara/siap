@extends('layout.layout')
@section('content')
<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>Form Satuan Kerja</h2>
                   <a href="{{ Url('pengajuan-absen-manual') }}" class="btn btn-info pull-right"><i class="fa fa-arrow-left"></i> Kembali</a>
					<div class="clearfix"></div>
            </div>
            <div class="x_content">
				@include('scripts.alert')
				{!! BootForm::horizontal(['model' => $model, 'store' => 'PengajuanAbsenManualController@store', 'update' => 'PengajuanAbsenManualController@update','enctype'=>'multipart/form-data']) !!}
				{!! BootForm::text('tanggal','Tanggal Dari',null,['class'=>'datepicker']) !!}
				<div class="form-group {{ !$errors->has('file') ?: 'has-error' }}">
					<label for="judul" class="control-label {{ config('bootstrap_form.left_column_class') }}">File Attachment</label>
					<div class="{{ config('bootstrap_form.right_column_class') }}">	
						<div class="input-group">
							<label class="input-group-btn">
								<span class="btn btn-primary">
									Browse&hellip; <input type="file" style="display: none;" name="file">
								</span>
							</label>
							<input type="text" class="form-control" readonly id="filename" required>
						</div>
						@if ($model->lampiran) <span class="help-block"><a href="{{ Illuminate\Support\Facades\Storage::url($model->lampiran) }}">Lampiran</a></span> @endif
						<span class="help-block">{{ $errors->first('file') }}</span>
					</div>
					
				</div>
				{!! BootForm::submit('Simpan') !!}
				{!! BootForm::close() !!}
			</div>
		</div>
	</div>
</div>
<script>
   var CURRENT_URL = "{{ Url('pengajuan-absen-manual') }}";
   $(function(){
	  $(document).on('change', ':file', function() {
        var input = $(this),
            numFiles = input.get(0).files ? input.get(0).files.length : 1,
            label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
			input.trigger('fileselect', [numFiles, label]);
      });
		
	  $(':file').on('fileselect', function(event, numFiles, label) {
		 $('#filename').val(label);
	  });
   })
</script>
@endsection