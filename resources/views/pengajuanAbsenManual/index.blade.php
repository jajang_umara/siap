@extends('layout.layout')

@section('content')

<div class="page-title">
    <div class="title_left">
        <h3>Data Pengajuan Absen Manual</h3>
    </div>
	
</div>
<div class="clearfix"></div>

<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>Data Pengajuan</h2>
                    <ul class="nav navbar-right panel_toolbox">
						<li><a href="{{ Url('pengajuan-absen-manual/create') }}" class="btn btn-info"><i class="fa fa-plus"></i> Tambah Data</a>&nbsp;&nbsp;</li>
					</ul>
					<div class="clearfix"></div>
            </div>
            <div class="x_content">
				<div class="col-md-8 col-md-offset-2">
					{!! BootForm::horizontal() !!}
						{!! BootForm::select('satuan_kerja',null,$satuanKerja) !!}
						{!! BootForm::button('<i class="fa fa-search"></i> Cari',['onclick'=>'doSearch()']) !!}
					{!! BootForm::close() !!}
				</div>
				<hr class="col-md-12">
				<div class="col-md-12">
					@include('scripts.alert')
				</div>
                <table id="myTable" class="table table-striped table-bordered dt-responsive" width="100%">
                    <thead>
						<tr>
                          <th>No</th>
						  <th>Satuan Kerja</th>
						  <th>User</th>
						  <th>Tanggal</th>
						  <th>status</th>
						  <th>Lampiran</th>
						  <th></th>
                         
                        </tr>
                    </thead>
					  
				</table>
			</div>
		</div>
	</div>
</div>
@include('scripts.datatable')
<script>
	
	$(function(){
		table = $('#myTable').DataTable({
			"processing": true,
			"serverSide": true,
			"ajax": {
				"url": "{{ Url('pengajuan-absen-manual/datatable') }}",
				"type":"POST",
				"data": function ( d ) {
					d.satuan_kerja_id = $('#satuan_kerja').val();
				}
			},
			"columns": [
				{ "data": 'DT_RowIndex',"searchable": false},
				{ "data": "satuan_kerja.nama_satuan_kerja" },
				{ "data": "user.username" },
				{ "data": "tanggal" },
				{ "data": "status"},
				{ "data": "lampiran"},
				{ "data": "action"}
			],
			"initComplete": function(settings, json) {
				$('[data-toggle="tooltip"]').tooltip();
			},
			"order": [[3, 'desc']],
		});
		
		$('#myTable').on('click', '.btn-delete', function (e) { 
			e.preventDefault();
			var id = $(this).data('id');
			$_confirm(function(){
				$.ajax({
					url: "{{ Url('pengajuan-absen-manual') }}/"+id,
					type: 'DELETE',
					dataType: 'json',
					data: {method: '_DELETE', submit: true}
				}).always(function (data) {
					if (data.responseText == 'success'){
						table.draw(false);
					} else {
						swal(data.responseText);
					}
				});
			});
		});
	});
	
	function doSearch()
	{
		table.draw(false);
	}
	
	function updateStatus(id,status)
	{
		$.confirm({
			title: 'Konfirmasi!',
			content: status == 1 ? 'Apakah Anda akan menyetujui pengajuan ini?' : 'Apakah Anda akan menolak pengajuan ini?',
			buttons: {
				confirm: function(){
					$.get("{{ Url('pengajuan-absen-manual/update-status') }}",{id:id,status:status}).done(function(result){
						if (result == 'success'){
							table.draw(false);
						} else {
							swal(result);
						}
					});
				},
				cancel: function () {
				},
			
			}
		});
	}
</script>

@endsection