@extends('layout.layout')
@section('content')
<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>Form Unit Kerja</h2>
                   <a href="{{ Url('unit-kerja') }}" class="btn btn-info pull-right"><i class="fa fa-arrow-left"></i> Kembali</a>
					<div class="clearfix"></div>
            </div>
            <div class="x_content">
				@if (session('status'))
					<div class="alert alert-success">
						{{ session('status') }}
					</div>
				@endif
				@if (session('error'))
					<div class="alert alert-danger">
						{{ session('error') }}
					</div>
				@endif
				{!! BootForm::horizontal(['model' => $model, 'store' => 'UnitKerjaController@store', 'update' => 'UnitKerjaController@update']) !!}
				{!! BootForm::select('satuan_kerja_id',null,$satuanKerja,null,['onchange'=>'satuanKerjaSelect(this.value)']) !!}
				{!! BootForm::text('nama_unit_kerja') !!}
				{!! BootForm::select('parent','Induk Unit Kerja') !!}
				{!! BootForm::submit('Simpan') !!}
				
				{!! BootForm::close() !!}
			</div>
		</div>
	</div>
</div>
<script>
   var CURRENT_URL = "{{ Url('unit-kerja') }}";
   var parentId = "{{ $model->parent }}";
   var containerSelect =  $('select[name="parent"]');
   var unitKerjaId;
   $(function(){
	  satuanKerjaSelect($('select[name="satuan_kerja_id"]').val()); 
   });
   
   function satuanKerjaSelect(value)
   {
	   $.get("{{ Url('unit-kerja/get-combobox-value') }}",{satuan_kerja_id:value}).done(function(result){
		   var result = JSON.parse(result);
		   containerSelect.empty();
		   containerSelect.append("<option value='0'>Tanpa Induk</option>");
		   optionTree(result);
	   }).fail(function(xhr){
		  swal(xhr.responseText) 
	   });
   }
   
   
   
   
</script>
@endsection