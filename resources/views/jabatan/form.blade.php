@extends('layout.layout')
@section('content')
<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>Form Satuan Kerja</h2>
                   <a href="{{ Url('jabatan') }}" class="btn btn-info pull-right"><i class="fa fa-arrow-left"></i> Kembali</a>
					<div class="clearfix"></div>
            </div>
            <div class="x_content">
				@if (session('status'))
					<div class="alert alert-success">
						{{ session('status') }}
					</div>
				@endif
				@if (session('error'))
					<div class="alert alert-danger">
						{{ session('error') }}
					</div>
				@endif
				{!! BootForm::horizontal(['model' => $model, 'store' => 'JabatanController@store', 'update' => 'JabatanController@update']) !!}
				{!! BootForm::text('kode_jabatan') !!}
				{!! BootForm::text('nama_jabatan') !!}
				{!! BootForm::select('satuan_kerja_id','Satuan Kerja',$satuanKerja,null,['onchange'=>'satuanKerjaSelect(this.value)']) !!}
				{!! BootForm::select('unit_kerja_id','Unit Kerja') !!}
				{!! BootForm::submit('Simpan') !!}
				{!! BootForm::close() !!}
			</div>
		</div>
	</div>
</div>
<script>
   var CURRENT_URL = "{{ Url('jabatan') }}";
   var parentId = "{{ $model->unit_kerja_id }}";
   var containerSelect =  $('select[name="unit_kerja_id"]');
   
   $(function(){
	  satuanKerjaSelect($('select[name="satuan_kerja_id"]').val()); 
   });
   
   function satuanKerjaSelect(value)
   {
	   $.get("{{ Url('unit-kerja/get-combobox-value') }}",{satuan_kerja_id:value}).done(function(result){
		   var result = JSON.parse(result);
		   containerSelect.empty();
		   containerSelect.append("<option value='0'>Pilih Unit Kerja</option>");
		   optionTree(result);
	   }).fail(function(xhr){
		  swal(xhr.responseText) 
	   });
   }
</script>
@endsection