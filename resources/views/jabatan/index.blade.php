@extends('layout.layout')

@section('content')

<div class="page-title">
    <div class="title_left">
        <h3>Master Jabatan</h3>
    </div>
	
</div>
<div class="clearfix"></div>

<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>Data Jabatan</h2>
                    <ul class="nav navbar-right panel_toolbox">
						<li><a href="{{ Url('jabatan/create') }}" class="btn btn-info"><i class="fa fa-plus"></i> Tambah Data</a>&nbsp;&nbsp;</li>
					</ul>
					<div class="clearfix"></div>
            </div>
            <div class="x_content">
                <table id="myTable" class="table table-striped table-bordered dt-responsive" width="100%">
                    <thead>
						<tr>
                          <th>No</th>
						  <th>Kode Jabatan</th>
						  <th>Nama Jabatan</th>
						  <th>Satuan Kerja</th>
						  <th>Unit Kerja</th>
						  <th></th>
                         
                        </tr>
                    </thead>
					  
				</table>
			</div>
		</div>
	</div>
</div>
@include('scripts.datatable')
<script>
	
	$(function(){
		table = $('#myTable').DataTable({
			"processing": true,
			"serverSide": true,
			"ajax": {
				"url": "{{ Url('jabatan/datatable') }}",
				"type":"POST",
				"data": function ( d ) {
					
				}
			},
			"columns": [
				{ "data": 'DT_RowIndex',"searchable": false},
				{ "data": "kode_jabatan" },
				{ "data": "nama_jabatan" },
				{ "data": "satuan_kerja.nama_satuan_kerja" },
				{ "data": "unit_kerja.nama_unit_kerja" },
				{ "data": "action"}
			],
			"initComplete": function(settings, json) {
				$('[data-toggle="tooltip"]').tooltip();
			},
			"order": [[1, 'asc']],
		});
		
		$('#myTable').on('click', '.btn-delete', function (e) { 
			e.preventDefault();
			var id = $(this).data('id');
			$_confirm(function(){
				$.ajax({
					url: "{{ Url('jabatan') }}/"+id,
					type: 'DELETE',
					dataType: 'json',
					data: {method: '_DELETE', submit: true}
				}).always(function (data) {
					if (data.responseText == 'success'){
						table.draw(false);
					} else {
						swal(data.responseText);
					}
				});
			});
		});
	});
</script>

@endsection