@extends('layout.layout')
@section('content')
<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>Form Pin Absen</h2>
                    <a href="{{ Url('pin-absen') }}" class="btn btn-info pull-right"><i class="fa fa-arrow-left"></i> Kembali</a>
					<div class="clearfix"></div>
            </div>
            <div class="x_content">
				@include('scripts.alert')
				{!! BootForm::horizontal(['model' => $model, 'store' => 'PinAbsenController@store', 'update' => 'PinAbsenController@update']) !!}
				{!! BootForm::text('nama_pegawai','Pegawai',@$model->pegawai->nama_pegawai,['suffix' => BootForm::addonButton('<i class="fa fa-search"></i>', ['class' => 'btn-success','onclick'=>'pegawaiData()']),'readonly'=>'readonly']) !!}
				{!! BootForm::hidden('pegawai_id') !!}
				{!! BootForm::text('pin_id','Pin Absen') !!}
				{!! BootForm::submit('Simpan') !!}
				{!! BootForm::close() !!}
			</div>
		</div>
	</div>
</div>
<script>
   var CURRENT_URL = "{{ Url('pin-absen') }}";
   var pegInput = $('#nama_pegawai');
   $(function(){
	  $('.do-search').click(function(){
		 loadTable(); 
	  });  
   });
</script>
@include('scripts.pegawaiData')
@endsection