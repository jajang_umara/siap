<?php
namespace App\Model;

use Illuminate\Database\Eloquent\Model;
class Jabatan extends Model {
	
	public $timestamps = false;
	protected $table = "data_jabatan";
	protected $primaryKey = 'jabatan_id';
	protected $fillable = ['nama_jabatan','kode_jabatan','satuan_kerja_id','unit_kerja_id'];
	
	public function asDropdown($where)
	{
		return $this->query()->where($where)->orderBy('kode_jabatan');
	}
	
	public function satuanKerja()
	{
		return $this->hasOne('App\Model\SatuanKerja','satuan_kerja_id','satuan_kerja_id');
	}
	
	public function unitKerja()
	{
		return $this->hasOne('App\Model\UnitKerja','unit_kerja_id','unit_kerja_id');
	}
}