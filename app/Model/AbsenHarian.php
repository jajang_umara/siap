<?php
namespace App\Model;

use Illuminate\Database\Eloquent\Model;
class AbsenHarian extends Model {
	
	public $timestamps = false;
	protected $table = "data_absen_harian";
	protected $fillable = ['pin_id','datetime','workcode'];
	
}