<?php
namespace App\Model;

use Illuminate\Database\Eloquent\Model;
class AbsenHarianTemp extends Model {
	
	public $timestamps = false;
	protected $table = "data_absen_harian_temp";
	protected $fillable = ['pin_id','datetime','workcode'];
	
}