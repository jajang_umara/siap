<?php
namespace App\Model;

use Illuminate\Database\Eloquent\Model;
class Pegawai extends Model {
	
	public $timestamps = false;
	protected $table = "data_pegawai";
	protected $primaryKey = 'pegawai_id';
	protected $fillable = ['nama_pegawai','nip_pegawai','jabatan_id','email_pegawai','satuan_kerja_id','unit_kerja_id'];
	
	static function asDropdown($request)
	{
		return Pegawai::query()->where('unit_kerja_id',$request->unit_kerja_id)->orderBy('nama_pegawai')->pluck('nama_pegawai','pegawai_id');
	}
	
	public function satuanKerja()
	{
		return $this->hasOne('App\Model\SatuanKerja','satuan_kerja_id','satuan_kerja_id');
	}
	
	public function unitKerja()
	{
		return $this->hasOne('App\Model\UnitKerja','unit_kerja_id','unit_kerja_id');
	}
}