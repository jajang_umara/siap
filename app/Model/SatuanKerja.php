<?php
namespace App\Model;

use Illuminate\Database\Eloquent\Model;
class SatuanKerja extends Model {
	
	public $timestamps = false;
	protected $table = "data_satuan_kerja";
	protected $primaryKey = 'satuan_kerja_id';
	protected $fillable = ['nama_satuan_kerja','alamat'];
	
	public function setNamaSatuanKerjaAttribute($value)
    {
        $this->attributes['nama_satuan_kerja'] = strtoupper($value);
    }
	
	public function asDropdown()
	{
		return $this->query()->orderBy('nama_satuan_kerja')->pluck('nama_satuan_kerja','satuan_kerja_id');
	}
	
	static function dropdown()
	{
		return SatuanKerja::query()->orderBy('nama_satuan_kerja')->pluck('nama_satuan_kerja','satuan_kerja_id');
	}
}