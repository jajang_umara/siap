<?php
namespace App\Model;

use Illuminate\Database\Eloquent\Model;
class JamKerjaPegawai extends Model {
	
	public $timestamps = false;
	protected $table = "data_jam_kerja_pegawai";
	
}