<?php
namespace App\Model;

use Illuminate\Database\Eloquent\Model;
class JamKerja extends Model {
	
	public $timestamps = false;
	protected $table = "data_jam_kerja";
	
	public function setCheckinStartAttribute($value)
    {
        $this->attributes['checkin_start'] = strtotime($value);
    }
	
	public function setCheckinEndAttribute($value)
    {
        $this->attributes['checkin_end'] = strtotime($value);
    }
	
	public function getCheckinStartAttribute($value)
    {
        return date('H:i',$value);
    }
	
	public function getCheckinEndAttribute($value)
    {
        return date('H:i',$value);
    }
	
	public function setCheckoutStartAttribute($value)
    {
        $this->attributes['checkout_start'] = strtotime($value);
    }
	
	public function setCheckoutEndAttribute($value)
    {
        $this->attributes['checkout_end'] = strtotime($value);
    }
	
	public function getCheckoutStartAttribute($value)
    {
        return date('H:i',$value);
    }
	
	public function getCheckoutEndAttribute($value)
    {
        return date('H:i',$value);
    }
	
	public function setIstirahatStartAttribute($value)
    {
        $this->attributes['istirahat_start'] = strtotime($value);
    }
	
	public function setIstirahatEndAttribute($value)
    {
        $this->attributes['istirahat_end'] = strtotime($value);
    }
	
	public function getIstirahatStartAttribute($value)
    {
        return date('H:i',$value);
    }
	
	public function getIstirahatEndAttribute($value)
    {
        return date('H:i',$value);
    }
	
}