<?php
namespace App\Model;

use Illuminate\Database\Eloquent\Model;
class UnitKerja extends Model {
	
	public $timestamps = false;
	protected $table = "data_unit_kerja";
	protected $primaryKey = 'unit_kerja_id';
	protected $fillable = ['nama_unit_kerja','satuan_kerja_id','parent'];
	private $satuanKerja;
	
	public function setNamaUnitKerjaAttribute($value)
    {
        $this->attributes['nama_unit_kerja'] = strtoupper($value);
    }
	
	public function buildTree($satuan_kerja_id,$parent = 0) {
		$tree = array();
		$data = $this->where('satuan_kerja_id',$satuan_kerja_id)->where('parent',$parent)->get()->toArray();
		foreach ($data as $d) {
        if ($d['parent'] == $parent) {
            $children = $this->buildTree($satuan_kerja_id,$d['unit_kerja_id']);
            // set a trivial key
            if (!empty($children)) {
					$d['_children'] = $children;
				}
				$tree[] = $d;
			}
		}
		return $tree;
	}
	
	public function getUnitKerja($request)
	{
		$unit = $this->query();
		if ($request->has('satuan_kerja_id')){
			$unit->where('satuan_kerja_id',$request->get('satuan_kerja_id'))->where('parent',0);
		}
		$unit->orderBy('nama_unit_kerja');
		return $unit->get()->toArray();
	}
	
	public function satuanKerja()
	{
		return $this->hasOne('App\Model\SatuanKerja','satuan_kerja_id','satuan_kerja_id');
	}
	
	public function parentData()
	{
		return $this->hasOne('App\Model\UnitKerja','unit_kerja_id','parent');
	}
	
	
}