<?php
namespace App\Model;

use Illuminate\Database\Eloquent\Model;
class PengajuanAbsenManual extends Model {
	
	public $timestamps = false;
	protected $table = "data_pengajuan_absen_manual";
	protected $fillable = ['user_id','satuan_kerja_id','user_id','tanggal_from','tanggal_to','lampiran'];
	
	public function satuanKerja()
	{
		return $this->hasOne('App\Model\SatuanKerja','satuan_kerja_id','satuan_kerja_id');
	}
	
	public function user()
	{
		return $this->hasOne('App\Model\Users','id','user_id');
	}
	
	
}