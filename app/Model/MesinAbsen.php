<?php
namespace App\Model;

use Illuminate\Database\Eloquent\Model;
class MesinAbsen extends Model {
	
	public $timestamps = false;
	protected $table = "data_mesin_absen";
	protected $primaryKey = 'mesin_id';
	protected $fillable = ['nama_mesin','merk','type','serial_number','ip_address'];
	
	static function asDropdown()
	{
		return (new MesinAbsen())->query()->orderBy('nama_mesin')->pluck('nama_mesin','ip_address');
	}
	
	static function getMesinIp()
	{
		return MesinAbsen::all()->pluck('ip_address');
	}
}