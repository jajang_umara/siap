<?php
namespace App\Model;

use Illuminate\Database\Eloquent\Model;
class HariLibur extends Model {
	
	public $timestamps = false;
	protected $table = "data_hari_libur";
	
}