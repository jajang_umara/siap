<?php
namespace App\Model;

use Illuminate\Database\Eloquent\Model;
class PinAbsen extends Model {
	public $timestamps = false;
	protected $table = "data_pin_absen";
	
	public function pegawai()
	{
		return $this->hasOne('App\Model\Pegawai','pegawai_id','pegawai_id');
	}
}

