<?php

namespace App\Services;

use App\Model\AbsenHarian;
use App\Model\AbsenHarianTemp;
use App\Model\MesinAbsen;
use App\Libraries\ZK\ZkLib;
class AttLog 
{
	
	static function getAttLog($ips = [],$dates = [])
	{
		$data = [];
		if (count($ips) == 0){
			$ips = MesinAbsen::getMesinIp();
		}
		
		if (count($dates) == 0){
			$dates[] = date('Y-m-d',strtotime('-1 day'))." 00:00:00";
			$dates[] = date('Y-m-d',strtotime('-1 day'))." 23:59:59";
		}
		//return $dates;
		
		foreach($ips as $ip){
			$zkLib = new ZkLib($ip);
			$attLogs = $zkLib->getAttLog();
			if ($rows = collect($attLogs['Row'])->whereBetween('DateTime',$dates)->toArray()){
				return $rows;
				foreach ( $rows as $row){
					
					$data[] = [
						'pin_id' => $row->PIN,
						'datetime' => $row->DateTime,
						'workcode' => $row->WorkCode,
						'status' => $row->status 
					];
				}
			}
			
		}
		//return json_encode($data);
		if (count($data) > 0){
			AbsenHarianTemp::query()->delete();
			AbsenHarian::insert($data);
			AbsenHarianTemp::insert($data);
		}
		
		return true;
	}
	
}