<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\SatuanKerja;

class SatuanKerjaController extends Controller
{
	
	public function index()
    {
        return view('satuanKerja.index');
    }
	
	public function datatable(Request $request)
	{
		$model = SatuanKerja::query();
		
		//$user->where('username','admin');
		
		return datatables()->eloquent($model)
			->addColumn('action', function ($model) {
                return '<a href="'.Url('satuan-kerja/'.$model->satuan_kerja_id.'/edit').'" class="btn btn-xs btn-primary edit"><i class="glyphicon glyphicon-edit"></i> Edit</a>
					    <button class="btn btn-xs btn-danger btn-delete" data-id="'.$model->satuan_kerja_id.'"><i class="glyphicon glyphicon-trash"></i> Delete</a>';
            })
			->addIndexColumn()
			->toJson();
	}

   
    public function create()
    {
        return view('satuanKerja.form',['satuanKerja'=> new SatuanKerja()]);
    }

  
    public function store(Request $request)
    {
        $request->validate([
			'nama_satuan_kerja' => 'required',
			'alamat' => 'required'
		]);
		
		
		if (SatuanKerja::create($request->all())){
			return redirect('satuan-kerja/create')->with('status','Data Berhasil Disimpan');
		} 
    }

 
    public function show($id)
    {
        
    }

   
    public function edit($id)
    {
        return view('satuanKerja.form',['satuanKerja'=> SatuanKerja::find($id)]);
    }

    
    public function update(Request $request, $id)
    {
        $satuanKerja = SatuanKerja::find($id);
		if (!$satuanKerja){
			return redirect("satuan-kerja/$id/edit")->with('error','Data Tidak ditemukan');
		}
		
		$satuanKerja->nama_satuan_kerja = $request->get('nama_satuan_kerja');
		$satuanKerja->alamat = $request->get('alamat');
		if ($satuanKerja->save()){
			return redirect("satuan-kerja/$id/edit")->with('status','Data Berhasil Diubah');
		}
    }

    
    public function destroy($id)
    {
        if (SatuanKerja::destroy($id)){
			return 'success';
		}
		return 'Data gagal Dihapus';
    }
}
