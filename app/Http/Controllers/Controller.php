<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
	
	protected function buildTree(Array $data, $parent = 0,$fields = ['id','parent']) {
		$tree = array();
		foreach ($data as $d) {
        if ($d[$fields[1]] == $parent) {
            $children = $this->buildTree($data, $d[$fields[0]],$fields);
            // set a trivial key
            if (!empty($children)) {
					$d['_children'] = $children;
				}
				$tree[] = $d;
			}
		}
		return $tree;
	}
}
