<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\Jabatan;
use App\Model\SatuanKerja;

class JabatanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('jabatan.index');
    }
	
	public function datatable(Request $request)
	{
		$model = Jabatan::query()->with('satuanKerja')->with('unitKerja');
		
		//$user->where('username','admin');
		
		return datatables()->eloquent($model)
			->addColumn('action', function ($model) {
                return '<a href="'.Url('jabatan/'.$model->jabatan_id.'/edit').'" class="btn btn-xs btn-primary edit"><i class="glyphicon glyphicon-edit"></i> Edit</a>
					    <button class="btn btn-xs btn-danger btn-delete" data-id="'.$model->jabatan_id.'"><i class="glyphicon glyphicon-trash"></i> Delete</a>';
            })
			->addIndexColumn()
			->toJson();
	}

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('jabatan.form',['model'=>new Jabatan(),'satuanKerja'=>(new SatuanKerja())->asDropdown()]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
			'kode_jabatan' => 'required|unique:data_jabatan',
			'nama_jabatan' => 'required',
			'satuan_kerja_id' => 'required',
			'unit_kerja_id' => 'required'
		]);
		
		if (Jabatan::create($request->all())){
			return redirect('jabatan/create')->with('status','Data Berhasil Disimpan');
		} else {
			return redirect('jabatan/create')->with('error','Data Gagal Disimpan');
		}
    }
	

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view('jabatan.form',['model'=>Jabatan::find($id),'satuanKerja'=>(new SatuanKerja())->asDropdown()]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
			'kode_jabatan' => 'required',
			'nama_jabatan' => 'required',
			'satuan_kerja_id' => 'required',
			'unit_kerja_id' => 'required'
		]);
		
		$jabatan = Jabatan::find($id);
		$jabatan->kode_jabatan = $request->get('kode_jabatan');
		$jabatan->nama_jabatan = $request->get('nama_jabatan');
		$jabatan->satuan_kerja_id = $request->get('satuan_kerja_id');
		$jabatan->unit_kerja_id = $request->get('unit_kerja_id');
		
		if ($jabatan->save()){
			return redirect("jabatan/$id/edit")->with('status','Data Berhasil Diubah');
		} else {
			return redirect("jabatan/$id/edit")->with('status','Data Gagal Diubah');
		}
    }

	public function destroy($id)
    {
        if (Jabatan::destroy($id)){
			return 'success';
		}
		return 'Data Gagal Dihapus';
    }
	
	public function comboboxValue(Request $request)
	{
		return (new Jabatan())->asDropdown(['unit_kerja_id'=>$request->get('unit_kerja_id')])->get()->toJson();
	}
}
