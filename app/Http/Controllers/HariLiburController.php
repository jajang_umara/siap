<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Model\HariLibur;

class HariLiburController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('hariLibur.index');
    }
	
	public function datatable()
    {
		$model = HariLibur::query();
		return datatables()->eloquent($model)
			->addColumn('action', function ($model) {
                return '<a href="'.Url('hari-libur/'.$model->id.'/edit').'" class="btn btn-xs btn-primary edit"><i class="glyphicon glyphicon-edit"></i> Edit</a>
					    <button class="btn btn-xs btn-danger btn-delete" data-id="'.$model->id.'"><i class="glyphicon glyphicon-trash"></i> Delete</a>';
            })
			->addIndexColumn()
			->toJson();
	}
	
    public function create()
    {
        return $this->form();
    }
	
	private function form($id=0)
	{
		$model = $id == 0 ? new HariLibur() : HariLibur::find($id);
		return view('hariLibur.form',['model'=>$model]);
	}

    private function upsert($request,$id=0 ,$edit=false)
	{
		$request->validate([
			'tanggal' => 'required',
			'keterangan' => 'required'
		]);
		
		if ($edit){
			$row = HariLibur::find($id);
		} else {
			$row = new HariLibur();
		}
		
		$auth  = Auth::user();
		
		$row->tanggal = $request->tanggal;
		$row->keterangan = $request->keterangan;
		$row->oleh = $auth->username;
		
		if ($row->save()){
			return redirect()
				->back()
				->with('status',"Data Berhasil Diproses");
		} else {
			return redirect()
				->back()
				->with('error','Data gagal diproses');
		}
	}
	
    public function store(Request $request)
    {
        return $this->upsert($request);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return $this->form($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        return $this->upsert($request,$id,true);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (HariLibur::destroy($id)){
			return 'success';
		}
		return 'Data gagal dihapus';
    }
}
