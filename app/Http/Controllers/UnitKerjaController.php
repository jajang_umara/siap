<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\UnitKerja;
use App\Model\SatuanKerja;


class UnitKerjaController extends Controller
{
    public function index()
    {
        return view('unitKerja.index');
    }

   
    public function create()
    {
        return view('unitKerja.form',['model'=>new UnitKerja(),
									  'satuanKerja'=>(new SatuanKerja())->asDropdown()]);
    }
	
	public function comboboxValue(Request $request)
	{
		return collect((new UnitKerja())->buildTree($request->get('satuan_kerja_id')))->toJson();
	}
	
	public function datatable()
	{
		$model = UnitKerja::query()->with('satuanKerja')->with('parentData');
		
		//$user->where('username','admin');
		
		return datatables()->eloquent($model)
			->addColumn('action', function ($model) {
                return '<a href="'.Url('unit-kerja/'.$model->unit_kerja_id.'/edit').'" class="btn btn-xs btn-primary edit"><i class="glyphicon glyphicon-edit"></i> Edit</a>
					    <button class="btn btn-xs btn-danger btn-delete" data-id="'.$model->unit_kerja_id.'"><i class="glyphicon glyphicon-trash"></i> Delete</a>';
            })
			->addIndexColumn()
			->toJson();
	}

    
    public function store(Request $request)
    {
         $request->validate([
			'nama_unit_kerja' => 'required',
			'satuan_kerja_id' => 'required',
			'parent' => 'required'
		]);
		
		if (UnitKerja::create($request->all())){
			return redirect('unit-kerja/create')->with('status','Data Berhasil Disimpan');
		} 
    }

   
    public function show($id)
    {
        //
    }

  
    public function edit($id)
    {
        return view('unitKerja.form',['model'=>UnitKerja::find($id),
									  'satuanKerja'=>(new SatuanKerja())->asDropdown()]);
    }

   
    public function update(Request $request, $id)
    {
         $request->validate([
			'nama_unit_kerja' => 'required',
			'satuan_kerja_id' => 'required',
			'parent' => 'required'
		]);
		
		$unit = UnitKerja::find($id);
		$unit->satuan_kerja_id = $request->get('satuan_kerja_id');
		$unit->nama_unit_kerja = $request->get('nama_unit_kerja');
		$unit->parent = $request->get('parent');
		
		if ($unit->save()){
			return redirect("unit-kerja/$id/edit")->with('status','Data Berhasil Diubah');
		} 
    }

   
    public function destroy($id)
    {
        if (UnitKerja::destroy($id)){
			return 'success';
		}
		return 'Data Gagal Dihapus';
    }
}
