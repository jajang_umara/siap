<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\SatuanKerja;
use App\Model\PinAbsen;

class PinAbsenController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('pinAbsen.index',['satuanKerja'=>SatuanKerja::dropdown()]);
    }

   public function datatable(Request $request)
    {
		$model = PinAbsen::query()->with('pegawai');
		
		//$user->where('username','admin');
		
		return datatables()->eloquent($model)
			->addColumn('action', function ($model) {
                return '<a href="'.Url('pin-absen/'.$model->id.'/edit').'" class="btn btn-xs btn-primary edit"><i class="glyphicon glyphicon-edit"></i> Edit</a>
					    <button class="btn btn-xs btn-danger btn-delete" data-id="'.$model->id.'"><i class="glyphicon glyphicon-trash"></i> Delete</a>';
            })
			->addIndexColumn()
			->toJson();
	}
	
    public function create()
    {
        return $this->form();
    }

    private function form($id=0)
	{
		$model = $id == 0 ? new PinAbsen() : PinAbsen::find($id);
		return view('pinAbsen.form',['model'=>$model,'satuanKerja'=>SatuanKerja::dropdown()]);
	}

    private function upsert($request,$id=0 ,$edit=false)
	{
		$request->validate([
			'nama_pegawai' => 'required',
			'pin_id' => 'required'
		]);
		
		if ($edit){
			$row = PinAbsen::find($id);
		} else {
			$row = new PinAbsen();
		}
		
		$row->pegawai_id = $request->pegawai_id;
		$row->pin_id = $request->pin_id;
		
		
		if ($row->save()){
			return redirect()
				->back()
				->with('status',"Data Berhasil Diproses");
		} else {
			return redirect()
				->back()
				->with('error','Data gagal diproses');
		}
	}
    public function store(Request $request)
    {
        return $this->upsert($request);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return $this->form($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        return $this->upsert($request,$id,true);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (PinAbsen::destroy($id)){
			return 'success';
		}
		return 'Data Gagal dihapus';
    }
}
