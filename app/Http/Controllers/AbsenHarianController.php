<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\AbsenHarian;
use App\Model\AbsenHarianTemp;
use App\Model\MesinAbsen;


class AbsenHarianController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       return view('absenHarian.index');
    }
	
	public function datatable()
    {
		$model = AbsenHarian::query();
		return datatables()->eloquent($model)
			->addIndexColumn()
			->toJson();
	}
	
	public function tarikAbsen(Request $request)
	{
		
	}

    
    public function create()
    {
        return view('absenHarian.form',['model'=>new AbsenHarian(),'mesin_absen'=>MesinAbsen::asDropdown()]);
    }

   
    public function store(Request $request)
    {
        //
    }

    
    public function show($id)
    {
        //
    }

   
    public function edit($id)
    {
        //
    }

   
    public function update(Request $request, $id)
    {
        //
    }

   
    public function destroy($id)
    {
        //
    }
}
