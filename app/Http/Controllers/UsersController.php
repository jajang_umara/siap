<?php
namespace App\Http\Controllers;

use App\Repositories\Users\UsersRepositoryInterface;
use Illuminate\Http\Request;
use App\UserClass;

class UsersController extends Controller {
	protected $user;
	
	public function __construct(UsersRepositoryInterface $users)
	{
		$this->user = $users;
	}
	
	public function index()
	{
		return View('users.index');
	}
	
	function show(Request $request)
	{
		$user = $this->user->datatable($request);
		
		//$user->where('username','admin');
		
		return datatables()->eloquent($user)
			->editColumn('proyek',function($user) {
				return implode(",",json_decode($user->proyek));
			})
			->addColumn('action', function ($user) {
                return '<a href="#edit-'.$user->id.'" class="btn btn-xs btn-primary edit" data-id="'.$user->id.'"><i class="glyphicon glyphicon-edit"></i> Edit</a>';
            })
			->toJson();
	}
	
	
}