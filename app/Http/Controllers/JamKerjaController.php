<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\JamKerja;

class JamKerjaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('jamKerja.index');
    }

    public function datatable(Request $request)
	{
		$model = JamKerja::query();
		
		//$user->where('username','admin');
		
		return datatables()->eloquent($model)
			->addColumn('action', function ($model) {
                return '<a href="'.Url('jam-kerja/'.$model->id.'/edit').'" class="btn btn-xs btn-primary edit"><i class="glyphicon glyphicon-edit"></i> Edit</a>
					    <button class="btn btn-xs btn-danger btn-delete" data-id="'.$model->id.'"><i class="glyphicon glyphicon-trash"></i> Delete</a>';
            })
			->addIndexColumn()
			->toJson();
	}
    public function create()
    {
        return $this->form();
    }
	
	private function form($id=0)
	{
		$model = $id == 0 ? new JamKerja() : JamKerja::find($id);
		return view('jamKerja.form',['model'=>$model]);
	}

    public function store(Request $request)
    {
        return $this->upsert($request);
    }

    private function upsert($request,$id=0 ,$edit=false)
	{
		$request->validate([
			'nama_jam_kerja' => 'required',
			'checkin_start' => 'required',
			'checkin_end' => 'required',
			'checkout_start' => 'required',
			'checkout_end' => 'required',
			'istirahat_start' => 'required',
			'istirahat_end' => 'required',
		]);
		
		if ($edit){
			$row = JamKerja::find($id);
		} else {
			$row = new JamKerja();
		}
		
		
		
		$row->nama_jam_kerja = $request->nama_jam_kerja;
		$row->checkin_start = $request->checkin_start;
		$row->checkin_end = $request->checkin_end;
		$row->checkout_start = $request->checkout_start;
		$row->checkout_end = $request->checkout_end;
		$row->istirahat_start = $request->istirahat_start;
		$row->istirahat_end = $request->istirahat_end;
		$row->is_default = $request->input('is_default',0);
		$row->is_crossday = $request->input('is_crossday',0);
		
		if ($row->is_default == 1){
			JamKerja::query()->update(['is_default'=>0]);
		}
		
		if ($row->save()){
			return redirect()
				->back()
				->with('status',"Data Berhasil Diproses");
		} else {
			return redirect()
				->back()
				->with('error','Data gagal diproses');
		}
	}
	
    public function show($id)
    {
        
    }

   
    public function edit($id)
    {
        return $this->form($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        return $this->upsert($request,$id,true);
    }

    
    public function destroy($id)
    {
        if (JamKerja::destroy($id)){
			return 'success';
		}
		
		return 'Data gagal dihapus';
    }
}
