<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\MesinAbsen;

class MesinAbsenController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('mesinAbsen.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('mesinAbsen.form',['model'=>new MesinAbsen()]);
    }
	
	public function datatable(Request $request)
	{
		$model = MesinAbsen::query();
		
		//$user->where('username','admin');
		
		return datatables()->eloquent($model)
			->addColumn('action', function ($model) {
                return '<a href="'.Url('mesin-absen/'.$model->mesin_id.'/edit').'" class="btn btn-xs btn-primary edit"><i class="glyphicon glyphicon-edit"></i> Edit</a>
					    <button class="btn btn-xs btn-danger btn-delete" data-id="'.$model->mesin_id.'"><i class="glyphicon glyphicon-trash"></i> Delete</a>';
            })
			->addIndexColumn()
			->toJson();
	}

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
		$request->validate([
			'nama_mesin' => 'required',
			'merk' => 'required',
			'type' => 'required',
			'serial_number' => 'required',
			'ip_address' => 'required|ip'
		]);
		
		if (MesinAbsen::create($request->all())){
			return redirect('mesin-absen/create')->with('status','Data Berhasil Disimpan');
		} else {
			return redirect('mesin-absen/create')->with('error','Data Gagal Disimpan');
		}
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view('mesinAbsen.form',['model'=>MesinAbsen::find($id)]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
			'nama_mesin' => 'required',
			'merk' => 'required',
			'type' => 'required',
			'serial_number' => 'required',
			'ip_address' => 'required|ip'
		]);
		
		$mesin = MesinAbsen::find($id);
		$mesin->nama_mesin = $request->get('nama_mesin');
		$mesin->merk = $request->get('merk');
		$mesin->type = $request->get('type');
		$mesin->serial_number = $request->get('serial_number');
		$mesin->ip_address = $request->get('ip_address');
		
		if ($mesin->save()){
			return redirect("mesin-absen/$id/edit")->with('status','Data Berhasil Diubah');
		} else {
			return redirect("mesin-absen/$id/edit")->with('error','Data Gagal Diubah');
		}
		
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (MesinAbsen::destroy($id)){
			return 'success';
		}
		return 'Data gagal Dihapus';
    }
}
