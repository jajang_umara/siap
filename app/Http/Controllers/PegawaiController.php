<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\Pegawai;
use App\Model\SatuanKerja;

class PegawaiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('pegawai.index');
    }
	
	public function datatable(Request $request)
	{
		$model = Pegawai::query()->with('satuanKerja')->with('unitKerja');
		
		//$user->where('username','admin');
		
		return datatables()->eloquent($model)
			->addColumn('action', function ($model) {
                return '<a href="'.Url('pegawai/'.$model->pegawai_id.'/edit').'" class="btn btn-xs btn-primary edit"><i class="glyphicon glyphicon-edit"></i> Edit</a>
					    <button class="btn btn-xs btn-danger btn-delete" data-id="'.$model->pegawai_id.'"><i class="glyphicon glyphicon-trash"></i> Delete</a>';
            })
			->addIndexColumn()
			->toJson();
	}
	
	public function comboboxValue(Request $req)
	{
		return Pegawai::asDropdown($req)->toJson();
	}
	
    public function create()
    {
        return view('pegawai.form',['model'=> new Pegawai(),'satuanKerja'=>(new SatuanKerja())->asDropdown()]);
    }

    public function listData(Request $req)
	{
		return Pegawai::query()->where('satuan_kerja_id',$req->satuan_kerja)->where(function($query) use($req){
			if ($req->has('unit_kerja') && $req->unit_kerja != ""){
				$query->where('unit_kerja_id',$req->unit_kerja);
			}
		})
		->limit(30)
		->with('satuanKerja')->get()->toJson();
	}
	
    public function store(Request $request)
    {
        $this->validateForm($request);
		if (Pegawai::create($request->all())){
			return redirect('pegawai/create')->with('status','Data Berhasil Disimpan');
		} else {
			return redirect('pegawai/create')->with('error','Data Gagal Disimpan');
		}
    }
	
	private function validateForm($request,$edit=false)
	{
		$request->validate([
			'nip_pegawai' => 'required'.$edit ? '' : '|unique:data_pegawai' ,
			'nama_pegawai' => 'required',
			'email_pegawai' => 'required|email',
			'jabatan_id' => 'required',
			'satuan_kerja_id' => 'required',
			'unit_kerja_id' => 'required'
		]);
		
		return $request;
	}

    
    public function show($id)
    {
        
    }

  
    public function edit($id)
    {
         return view('pegawai.form',['model'=> Pegawai::find($id),'satuanKerja'=>(new SatuanKerja())->asDropdown()]);
    }

  
    public function update(Request $request, $id)
    {
       $this->validateform($request,true);
		//return collect($request->except(['_method','_token']))->toJson();
		$pegawai = Pegawai::find($id);
		
		$pegawai->nama_pegawai = $request->get('nama_pegawai');
		$pegawai->email_pegawai = $request->get('email_pegawai');
		$pegawai->jabatan_id = $request->get('pegawai_id');
		$pegawai->satuan_kerja_id = $request->get('satuan_kerja_id');
		$pegawai->unit_kerja_id = $request->get('unit_kerja_id');
		
		if ($pegawai->save()){
			return redirect("pegawai/$id/edit")->with('status','Data Berhasil Diubah');
		} else {
			return redirect("pegawai/$id/edit")->with('error','Data Gagal Diubah');
		}
		 
    }

   
    public function destroy($id)
    {
        if (Pegawai::destroy($id)){
			return 'success';
		}
		
		return 'Data gagal dihapus';
    }
}
