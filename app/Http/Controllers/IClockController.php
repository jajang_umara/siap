<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;


class IClockController extends Controller
{
    

    public function cdataget(Request $req)
    {
        $sn = $req->get('SN','');
        return
			"GET OPTION FROM: $sn
			Stamp=0
			OpStamp=0
			ErrorDelay=60
			Delay=30
			TransTimes=00:00;14:05
			TransInterval=1
			TransFlag=1111000000
			TimeZone=7
			Realtime=1
			Encrypt=0";
    }

 }
