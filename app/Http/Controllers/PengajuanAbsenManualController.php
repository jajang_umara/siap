<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\PengajuanAbsenManual as Pengajuan;
use App\Model\SatuanKerja;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class PengajuanAbsenManualController extends Controller
{
    
    public function index()
    {
        return view('pengajuanAbsenManual.index',['satuanKerja'=>(new SatuanKerja())->asDropdown()]);
    }

   public function datatable(Request $request)
	{
		$model = Pengajuan::query()->with('satuanKerja')->with('user');
		
		$auth = Auth::user();
		
		if ($auth->role == 'operator'){
			$model->where('user_id',$auth->id);
		}
		
		return datatables()->eloquent($model)
			->addColumn('action', function ($model) use($auth) {
				$action = "";
				if ($auth->role == 'admin'){
						$action .= '<button class="btn btn-success btn-xs" title="Ok" onclick="updateStatus('.$model->id.',1)"><i class="fa fa-check"></i></button>
								 <button class="btn btn-danger btn-xs" title="Tolak" onclick="updateStatus('.$model->id.',2)"><i class="fa fa-times"></i></button> ';
					
					
				}
				if ($model->status == 0){
					$action .=  '<a href="'.Url('pengajuan-absen-manual/'.$model->id.'/edit').'" class="btn btn-xs btn-primary edit" title="Ubah"><i class="glyphicon glyphicon-edit"></i></a>
								<button class="btn btn-xs btn-danger btn-delete" data-id="'.$model->id.'" title="Hapus"><i class="glyphicon glyphicon-trash"></i></a>';
				}
				return $action;
            })
			->addIndexColumn()
			->editColumn('lampiran',function(Pengajuan $row){
				return '<a href="'.Storage::url($row->lampiran).'" target="_blank">Lampiran</a>';
			})
			->editColumn('status',function(Pengajuan $row){
				if ($row->status == 0){
					return '<span class="label label-default">Belum Diproses</span>';
				} else if($row->status == 1){
					return '<span class="label label-success">OK</span>';
				} else {
					return '<span class="label label-danger">Ditolak</span>';
				}
			})
			->escapeColumns([])
			->toJson();
	}
	
	private function form($id=0)
	{
		$model = $id == 0 ? new Pengajuan() : Pengajuan::find($id);
		if ($id != 0){
			if ($model->status != 0){
				return redirect()->back()->with('error','Data Tidak bisa diubah, karena sudah disetujui');
			}
		}
		
		return view('pengajuanAbsenManual.form',['model'=>$model]);
	}
	
    public function create()
    {
        return $this->form();
    }
	
	public function store(Request $request)
    {
		return $this->upsert($request);
    }
	
	private function upsert($request,$id=0 ,$edit=false)
	{
		$request->validate([
			'file' => $edit ? '' : 'required|mimes:pdf,jpg,png',
			'tanggal' => 'required',
		]);
		
		if ($edit){
			$row = Pengajuan::find($id);
		} else {
			$row = new Pengajuan();
		}
		
		$row->tanggal = $request->get('tanggal');
		if (!$edit) {
			$auth  = Auth::user();
			$row->user_id = $auth->id;
			$row->satuan_kerja_id = $auth->satuan_kerja_id;
		}
		
		if ($request->file('file')){
			$uploadedFile = $request->file('file');        
			$path = $uploadedFile->storeAs('public/files',$uploadedFile->getClientOriginalName());
			$row->lampiran = $path;
		}
		
		if ($row->save()){
			return redirect()
				->back()
				->with('status',"Data Berhasil Diproses");
		} else {
			return redirect()
				->back()
				->with('error','Data gagal diproses');
		}
	}

    public function edit($id)
    {
		return $this->form($id);
    }

    
    public function update(Request $request, $id)
    {
       return $this->upsert($request,$id,true);
    }

   
    public function destroy($id)
    {
        if ($row = Pengajuan::find($id)){
			
			if ($row->status != 0){
				return 'Data Tidak Bisa dihapus karena sudah Disetujui';
			}
			
			if ($row->delete()){
				Storage::delete($row->lampiran);
				return 'success';
			}
		}
		return 'Data Gagal dihapus';
    }
	
	public function updateStatus(Request $request)
	{
		if ($request->ajax()){
			$row = Pengajuan::find($request->id);
			$row->status = $request->status;
			if ($row->save()) {
				return 'success';
			}
			return 'Data gagal diproses';
		}
		
	}
	
	public function show($id)
	{
		return $id;
	}
}
