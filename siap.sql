-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 08, 2019 at 10:54 AM
-- Server version: 10.1.37-MariaDB
-- PHP Version: 7.3.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `siap`
--

-- --------------------------------------------------------

--
-- Table structure for table `data_absen_harian`
--

CREATE TABLE `data_absen_harian` (
  `id` bigint(20) NOT NULL,
  `pin_id` int(11) NOT NULL DEFAULT '0',
  `datetime` datetime NOT NULL,
  `workcode` int(11) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `data_absen_harian`
--

INSERT INTO `data_absen_harian` (`id`, `pin_id`, `datetime`, `workcode`, `status`) VALUES
(1, 50052, '2019-03-06 06:51:50', 0, 0),
(2, 50052, '2019-03-06 06:51:52', 0, 0),
(3, 50389, '2019-03-06 07:19:36', 0, 0),
(4, 50389, '2019-03-06 07:22:16', 0, 0),
(5, 51042, '2019-03-06 07:39:41', 0, 0),
(6, 50419, '2019-03-06 07:40:00', 0, 0),
(7, 50258, '2019-03-06 07:47:43', 0, 0),
(8, 50589, '2019-03-06 07:48:17', 0, 0),
(9, 50761, '2019-03-06 07:54:24', 0, 0),
(10, 50930, '2019-03-06 07:56:46', 0, 0),
(11, 51024, '2019-03-06 07:57:39', 0, 0),
(12, 21148, '2019-03-06 07:58:46', 0, 0),
(13, 21148, '2019-03-06 07:58:51', 0, 0),
(14, 51038, '2019-03-06 08:00:32', 0, 0),
(15, 50819, '2019-03-06 08:00:47', 0, 0),
(16, 50893, '2019-03-06 08:07:53', 0, 0),
(17, 50441, '2019-03-06 08:14:41', 0, 0),
(18, 50032, '2019-03-06 08:18:59', 0, 0),
(19, 50549, '2019-03-06 10:11:45', 0, 0),
(20, 50549, '2019-03-06 17:02:20', 0, 0),
(21, 50930, '2019-03-06 17:06:26', 0, 0),
(22, 50537, '2019-03-06 17:07:26', 0, 0),
(23, 50761, '2019-03-06 17:08:47', 0, 0),
(24, 50052, '2019-03-06 17:19:17', 0, 0),
(25, 50052, '2019-03-06 17:19:19', 0, 0),
(26, 50258, '2019-03-06 17:19:23', 0, 0),
(27, 51042, '2019-03-06 17:19:26', 0, 0),
(28, 51042, '2019-03-06 17:19:28', 0, 0),
(29, 51038, '2019-03-06 17:22:53', 0, 0),
(30, 50031, '2019-03-06 17:25:36', 0, 0),
(31, 50419, '2019-03-06 17:38:00', 0, 0),
(32, 50893, '2019-03-06 17:41:33', 0, 0),
(33, 51024, '2019-03-06 17:41:36', 0, 0),
(34, 50877, '2019-03-06 17:41:41', 0, 0),
(35, 50629, '2019-03-06 18:11:05', 0, 0),
(36, 50441, '2019-03-06 18:19:12', 0, 0),
(37, 50819, '2019-03-06 18:24:14', 0, 0),
(38, 50032, '2019-03-06 18:24:17', 0, 0),
(39, 50589, '2019-03-06 18:33:03', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `data_absen_harian_temp`
--

CREATE TABLE `data_absen_harian_temp` (
  `id` bigint(20) NOT NULL,
  `pin_id` int(11) NOT NULL DEFAULT '0',
  `datetime` datetime NOT NULL,
  `workcode` int(11) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `data_absen_harian_temp`
--

INSERT INTO `data_absen_harian_temp` (`id`, `pin_id`, `datetime`, `workcode`, `status`) VALUES
(1, 50052, '2019-03-06 06:51:50', 0, 0),
(2, 50052, '2019-03-06 06:51:52', 0, 0),
(3, 50389, '2019-03-06 07:19:36', 0, 0),
(4, 50389, '2019-03-06 07:22:16', 0, 0),
(5, 51042, '2019-03-06 07:39:41', 0, 0),
(6, 50419, '2019-03-06 07:40:00', 0, 0),
(7, 50258, '2019-03-06 07:47:43', 0, 0),
(8, 50589, '2019-03-06 07:48:17', 0, 0),
(9, 50761, '2019-03-06 07:54:24', 0, 0),
(10, 50930, '2019-03-06 07:56:46', 0, 0),
(11, 51024, '2019-03-06 07:57:39', 0, 0),
(12, 21148, '2019-03-06 07:58:46', 0, 0),
(13, 21148, '2019-03-06 07:58:51', 0, 0),
(14, 51038, '2019-03-06 08:00:32', 0, 0),
(15, 50819, '2019-03-06 08:00:47', 0, 0),
(16, 50893, '2019-03-06 08:07:53', 0, 0),
(17, 50441, '2019-03-06 08:14:41', 0, 0),
(18, 50032, '2019-03-06 08:18:59', 0, 0),
(19, 50549, '2019-03-06 10:11:45', 0, 0),
(20, 50549, '2019-03-06 17:02:20', 0, 0),
(21, 50930, '2019-03-06 17:06:26', 0, 0),
(22, 50537, '2019-03-06 17:07:26', 0, 0),
(23, 50761, '2019-03-06 17:08:47', 0, 0),
(24, 50052, '2019-03-06 17:19:17', 0, 0),
(25, 50052, '2019-03-06 17:19:19', 0, 0),
(26, 50258, '2019-03-06 17:19:23', 0, 0),
(27, 51042, '2019-03-06 17:19:26', 0, 0),
(28, 51042, '2019-03-06 17:19:28', 0, 0),
(29, 51038, '2019-03-06 17:22:53', 0, 0),
(30, 50031, '2019-03-06 17:25:36', 0, 0),
(31, 50419, '2019-03-06 17:38:00', 0, 0),
(32, 50893, '2019-03-06 17:41:33', 0, 0),
(33, 51024, '2019-03-06 17:41:36', 0, 0),
(34, 50877, '2019-03-06 17:41:41', 0, 0),
(35, 50629, '2019-03-06 18:11:05', 0, 0),
(36, 50441, '2019-03-06 18:19:12', 0, 0),
(37, 50819, '2019-03-06 18:24:14', 0, 0),
(38, 50032, '2019-03-06 18:24:17', 0, 0),
(39, 50589, '2019-03-06 18:33:03', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `data_hari_libur`
--

CREATE TABLE `data_hari_libur` (
  `id` int(11) NOT NULL,
  `tanggal` date NOT NULL,
  `keterangan` varchar(255) NOT NULL,
  `oleh` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `data_hari_libur`
--

INSERT INTO `data_hari_libur` (`id`, `tanggal`, `keterangan`, `oleh`) VALUES
(1, '2019-03-07', 'Hari nyepi pisan', 'admin');

-- --------------------------------------------------------

--
-- Table structure for table `data_jabatan`
--

CREATE TABLE `data_jabatan` (
  `jabatan_id` int(11) NOT NULL,
  `kode_jabatan` varchar(100) NOT NULL,
  `nama_jabatan` varchar(255) NOT NULL,
  `satuan_kerja_id` int(11) NOT NULL,
  `unit_kerja_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `data_jabatan`
--

INSERT INTO `data_jabatan` (`jabatan_id`, `kode_jabatan`, `nama_jabatan`, `satuan_kerja_id`, `unit_kerja_id`) VALUES
(1, '1.2.3.4', 'kepala keamanan', 3, 4);

-- --------------------------------------------------------

--
-- Table structure for table `data_jam_kerja`
--

CREATE TABLE `data_jam_kerja` (
  `id` int(11) NOT NULL,
  `nama_jam_kerja` varchar(255) NOT NULL,
  `checkin_start` varchar(100) NOT NULL,
  `checkin_end` varchar(100) NOT NULL,
  `checkout_start` varchar(100) NOT NULL,
  `checkout_end` varchar(100) NOT NULL,
  `is_default` int(11) NOT NULL DEFAULT '0',
  `istirahat_start` varchar(100) NOT NULL,
  `istirahat_end` varchar(100) NOT NULL,
  `is_crossday` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `data_jam_kerja`
--

INSERT INTO `data_jam_kerja` (`id`, `nama_jam_kerja`, `checkin_start`, `checkin_end`, `checkout_start`, `checkout_end`, `is_default`, `istirahat_start`, `istirahat_end`, `is_crossday`) VALUES
(1, 'jam kerja normal', '1551834000', '1551848400', '1551848460', '1551861000', 0, '1551848400', '1551852000', 1);

-- --------------------------------------------------------

--
-- Table structure for table `data_jam_kerja_pegawai`
--

CREATE TABLE `data_jam_kerja_pegawai` (
  `id` int(11) NOT NULL,
  `pegawai_id` int(11) NOT NULL,
  `jam_kerja_id` int(11) NOT NULL,
  `tanggal` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `data_mesin_absen`
--

CREATE TABLE `data_mesin_absen` (
  `mesin_id` int(11) NOT NULL,
  `nama_mesin` varchar(100) NOT NULL,
  `merk` varchar(100) NOT NULL,
  `type` varchar(100) NOT NULL,
  `serial_number` varchar(100) NOT NULL,
  `ip_address` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `data_mesin_absen`
--

INSERT INTO `data_mesin_absen` (`mesin_id`, `nama_mesin`, `merk`, `type`, `serial_number`, `ip_address`) VALUES
(1, 'mesin 1', 'solution', 'x-100c12', '11223344556677889910', '192.168.0.12');

-- --------------------------------------------------------

--
-- Table structure for table `data_pegawai`
--

CREATE TABLE `data_pegawai` (
  `pegawai_id` int(11) NOT NULL,
  `nip_pegawai` varchar(100) NOT NULL,
  `nama_pegawai` varchar(255) NOT NULL,
  `satuan_kerja_id` int(11) NOT NULL,
  `unit_kerja_id` int(11) NOT NULL,
  `jabatan_id` int(11) NOT NULL,
  `email_pegawai` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `data_pegawai`
--

INSERT INTO `data_pegawai` (`pegawai_id`, `nip_pegawai`, `nama_pegawai`, `satuan_kerja_id`, `unit_kerja_id`, `jabatan_id`, `email_pegawai`) VALUES
(1, '122334455', 'Jajang umara', 3, 4, 1, 'jajang.umara@gmail.com');

-- --------------------------------------------------------

--
-- Table structure for table `data_pengajuan_absen_manual`
--

CREATE TABLE `data_pengajuan_absen_manual` (
  `id` int(11) NOT NULL,
  `satuan_kerja_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `tanggal` date NOT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `lampiran` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `data_pengajuan_absen_manual`
--

INSERT INTO `data_pengajuan_absen_manual` (`id`, `satuan_kerja_id`, `user_id`, `tanggal`, `status`, `lampiran`) VALUES
(2, 2, 1, '2019-03-06', 2, 'public/files/JAJANGUM0709_1090398902.pdf');

-- --------------------------------------------------------

--
-- Table structure for table `data_pin_absen`
--

CREATE TABLE `data_pin_absen` (
  `id` int(11) NOT NULL,
  `pegawai_id` int(11) NOT NULL,
  `pin_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `data_satuan_kerja`
--

CREATE TABLE `data_satuan_kerja` (
  `satuan_kerja_id` int(11) NOT NULL,
  `nama_satuan_kerja` varchar(100) NOT NULL,
  `alamat` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `data_satuan_kerja`
--

INSERT INTO `data_satuan_kerja` (`satuan_kerja_id`, `nama_satuan_kerja`, `alamat`) VALUES
(2, 'DINAS PERHUBUNGAN', 'Jalan jalan 12345'),
(3, 'DINAS KEHUTANAN', 'jalan hutan');

-- --------------------------------------------------------

--
-- Table structure for table `data_unit_kerja`
--

CREATE TABLE `data_unit_kerja` (
  `unit_kerja_id` int(11) NOT NULL,
  `nama_unit_kerja` varchar(255) NOT NULL,
  `satuan_kerja_id` int(11) NOT NULL,
  `parent` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `data_unit_kerja`
--

INSERT INTO `data_unit_kerja` (`unit_kerja_id`, `nama_unit_kerja`, `satuan_kerja_id`, `parent`) VALUES
(1, 'CAMAT 1234', 2, 0),
(2, 'Sub Camat 1', 2, 1),
(3, 'Sub Camat 2', 2, 1),
(4, 'BIDANG PEMBINAAN PEGAWAI', 3, 0),
(6, 'camat 2', 2, 0);

-- --------------------------------------------------------

--
-- Table structure for table `data_users`
--

CREATE TABLE `data_users` (
  `id` int(11) NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(255) NOT NULL,
  `remember_token` varchar(100) NOT NULL,
  `satuan_kerja_id` int(11) NOT NULL,
  `role` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `data_users`
--

INSERT INTO `data_users` (`id`, `username`, `password`, `remember_token`, `satuan_kerja_id`, `role`) VALUES
(1, 'admin', '$2y$10$HdV2pfOTLYnPJall2oPKPOyN/fdc4gQzIMZc8MxBzXwVLwQvnERVG', 'mMnCFVvdqpiCjvFcKt91XO4FOEsK8zHWEgZgaLKLNkgzvNSX96sCo6CgvgUM', 2, 'admin');

-- --------------------------------------------------------

--
-- Table structure for table `jobs`
--

CREATE TABLE `jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `queue` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `attempts` tinyint(3) UNSIGNED NOT NULL,
  `reserved_at` int(10) UNSIGNED DEFAULT NULL,
  `available_at` int(10) UNSIGNED NOT NULL,
  `created_at` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `jobs`
--

INSERT INTO `jobs` (`id`, `queue`, `payload`, `attempts`, `reserved_at`, `available_at`, `created_at`) VALUES
(1, 'default', '{\"displayName\":\"App\\\\Jobs\\\\AbsensiPegawai\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\AbsensiPegawai\",\"command\":\"O:23:\\\"App\\\\Jobs\\\\AbsensiPegawai\\\":7:{s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";N;s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1552027540, 1552027540);

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `data_absen_harian`
--
ALTER TABLE `data_absen_harian`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `data_absen_harian_temp`
--
ALTER TABLE `data_absen_harian_temp`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `data_hari_libur`
--
ALTER TABLE `data_hari_libur`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `tanggal` (`tanggal`);

--
-- Indexes for table `data_jabatan`
--
ALTER TABLE `data_jabatan`
  ADD PRIMARY KEY (`jabatan_id`);

--
-- Indexes for table `data_jam_kerja`
--
ALTER TABLE `data_jam_kerja`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `data_jam_kerja_pegawai`
--
ALTER TABLE `data_jam_kerja_pegawai`
  ADD PRIMARY KEY (`id`),
  ADD KEY `jam_kerja_id` (`jam_kerja_id`),
  ADD KEY `pegawai_id` (`pegawai_id`);

--
-- Indexes for table `data_mesin_absen`
--
ALTER TABLE `data_mesin_absen`
  ADD PRIMARY KEY (`mesin_id`);

--
-- Indexes for table `data_pegawai`
--
ALTER TABLE `data_pegawai`
  ADD PRIMARY KEY (`pegawai_id`),
  ADD UNIQUE KEY `nip_pegawai` (`nip_pegawai`),
  ADD KEY `satuan_kerja_id` (`satuan_kerja_id`),
  ADD KEY `unit_kerja_id` (`unit_kerja_id`),
  ADD KEY `jabatan_id` (`jabatan_id`);

--
-- Indexes for table `data_pengajuan_absen_manual`
--
ALTER TABLE `data_pengajuan_absen_manual`
  ADD PRIMARY KEY (`id`),
  ADD KEY `satuan_kerja_id` (`satuan_kerja_id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `data_pin_absen`
--
ALTER TABLE `data_pin_absen`
  ADD PRIMARY KEY (`id`),
  ADD KEY `pegawai_id` (`pegawai_id`);

--
-- Indexes for table `data_satuan_kerja`
--
ALTER TABLE `data_satuan_kerja`
  ADD PRIMARY KEY (`satuan_kerja_id`);

--
-- Indexes for table `data_unit_kerja`
--
ALTER TABLE `data_unit_kerja`
  ADD PRIMARY KEY (`unit_kerja_id`),
  ADD KEY `satuan_kerja_id` (`satuan_kerja_id`);

--
-- Indexes for table `data_users`
--
ALTER TABLE `data_users`
  ADD PRIMARY KEY (`id`),
  ADD KEY `satuan_kerja_id` (`satuan_kerja_id`);

--
-- Indexes for table `jobs`
--
ALTER TABLE `jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `data_absen_harian`
--
ALTER TABLE `data_absen_harian`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=40;

--
-- AUTO_INCREMENT for table `data_absen_harian_temp`
--
ALTER TABLE `data_absen_harian_temp`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=40;

--
-- AUTO_INCREMENT for table `data_hari_libur`
--
ALTER TABLE `data_hari_libur`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `data_jabatan`
--
ALTER TABLE `data_jabatan`
  MODIFY `jabatan_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `data_jam_kerja`
--
ALTER TABLE `data_jam_kerja`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `data_jam_kerja_pegawai`
--
ALTER TABLE `data_jam_kerja_pegawai`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `data_mesin_absen`
--
ALTER TABLE `data_mesin_absen`
  MODIFY `mesin_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `data_pegawai`
--
ALTER TABLE `data_pegawai`
  MODIFY `pegawai_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `data_pengajuan_absen_manual`
--
ALTER TABLE `data_pengajuan_absen_manual`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `data_pin_absen`
--
ALTER TABLE `data_pin_absen`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `data_satuan_kerja`
--
ALTER TABLE `data_satuan_kerja`
  MODIFY `satuan_kerja_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `data_unit_kerja`
--
ALTER TABLE `data_unit_kerja`
  MODIFY `unit_kerja_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `data_users`
--
ALTER TABLE `data_users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `jobs`
--
ALTER TABLE `jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `data_jam_kerja_pegawai`
--
ALTER TABLE `data_jam_kerja_pegawai`
  ADD CONSTRAINT `data_jam_kerja_pegawai_ibfk_1` FOREIGN KEY (`jam_kerja_id`) REFERENCES `data_jam_kerja` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `data_jam_kerja_pegawai_ibfk_2` FOREIGN KEY (`pegawai_id`) REFERENCES `data_pegawai` (`pegawai_id`) ON UPDATE CASCADE;

--
-- Constraints for table `data_pegawai`
--
ALTER TABLE `data_pegawai`
  ADD CONSTRAINT `data_pegawai_ibfk_1` FOREIGN KEY (`satuan_kerja_id`) REFERENCES `data_satuan_kerja` (`satuan_kerja_id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `data_pegawai_ibfk_2` FOREIGN KEY (`unit_kerja_id`) REFERENCES `data_unit_kerja` (`unit_kerja_id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `data_pegawai_ibfk_3` FOREIGN KEY (`jabatan_id`) REFERENCES `data_jabatan` (`jabatan_id`) ON UPDATE CASCADE;

--
-- Constraints for table `data_pengajuan_absen_manual`
--
ALTER TABLE `data_pengajuan_absen_manual`
  ADD CONSTRAINT `data_pengajuan_absen_manual_ibfk_1` FOREIGN KEY (`satuan_kerja_id`) REFERENCES `data_satuan_kerja` (`satuan_kerja_id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `data_pengajuan_absen_manual_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `data_users` (`id`) ON UPDATE CASCADE;

--
-- Constraints for table `data_pin_absen`
--
ALTER TABLE `data_pin_absen`
  ADD CONSTRAINT `data_pin_absen_ibfk_1` FOREIGN KEY (`pegawai_id`) REFERENCES `data_pegawai` (`pegawai_id`) ON UPDATE CASCADE;

--
-- Constraints for table `data_unit_kerja`
--
ALTER TABLE `data_unit_kerja`
  ADD CONSTRAINT `data_unit_kerja_ibfk_1` FOREIGN KEY (`satuan_kerja_id`) REFERENCES `data_satuan_kerja` (`satuan_kerja_id`) ON UPDATE CASCADE;

--
-- Constraints for table `data_users`
--
ALTER TABLE `data_users`
  ADD CONSTRAINT `data_users_ibfk_1` FOREIGN KEY (`satuan_kerja_id`) REFERENCES `data_satuan_kerja` (`satuan_kerja_id`) ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
